export const local = 'https://quantum.mia-ps.inrae.fr/po2-ws';
// export const local = 'https://icotest.iate.inra.fr/po2-ws';
// export const local = 'http://localhost:8080';

/** Params :
login : le login du user (optionnel),
password : le password du user (optionnel);
renvoi la liste des repos (accès libre + accès restreint si login + mdp)
 */
export const listRepository = '/listRepository';
/** Params :
id : les ids des repo à fédérer (obligatoire),
session : id de la session SPO²Q (obligatoire, permet l'unicité du repository fédéré),
login : le login du user (optionnel),
password : le password du user (optionnel);
renvoi LE repository à interroger
 */
export const getRepository = '/getRepository';
/** Params :
id : l'id du repo federer à supprimer (obligatoire),
login : le login du user (optionnel),
password : le password du user (optionnel);
supprime le repository fédérer (à appeler à la fin de la session SPO²Q) */
// export const deleteRepository = '/deleteRepository';

export const logout = '/logout';
export const login = '/authentication';

export const getSaveList = '/getSaveList';

/**
 * save : string la sauvegarde
 * saveName : string nom de la sauvegarde
 * saveDesc : string description de la sauvegarde
 */
export const saveFormState = '/saveFormState';
export const saveEditorState = '/saveEditorState';
export const saveWholeState = '/saveWholeState';

/** Params :
 unit : le code UCUM de l'unité a tester,
 return 400 sur echec
 retourn 200 true si l'unité est OK
*/
export const checkUnit = '/checkUnit';

/** Params :
 unit : le code UCUM de l'unité a convertire,
 value : la valeur a convertire
 return 400 sur echec mauvaise valeur ou unit pas un code ucum valide
 retourn 200 "unit" l'unité convertie, "value" : la valeur convertie
 */
export const getSIQuantity = '/getSIQuantity';

/** Params :
 // tslint:disable-next-line:jsdoc-format
 no parameter
 retourn 200 json with all units
 */
export const getAllUnits = '/getAllUnits';

/** Params :

 */
export const publishDataverse = '/publishDataset';
export const updateDataverse = '/updateDataset';

export const getDatasetList = '/listDataset';
