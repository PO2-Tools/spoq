import { Component, OnInit, ViewChild } from '@angular/core';
import { ExecuteRequestService } from '../services/execute-request.service';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { Sort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { RESULTS } from '../mock/result-data-stub';
import { SpoqFormatOptionsComponent } from '../spoq-format-options/spoq-format-options.component';
import { Router } from '@angular/router';
import {SessionService} from '../services/session.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {PublishDataverseComponent} from '../publish-dataverse/publish-dataverse.component';

@Component({
  selector: 'app-spoq-results-page',
  templateUrl: './spoq-results-page.component.html',
  styleUrls: ['./spoq-results-page.component.css']
})
export class SpoqResultsPageComponent implements OnInit {
  // MatPaginator Inputs
  @ViewChild(MatTable) table: MatTable<any>;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100, 250, 1000];
  sortOrder: Sort;
  start: number;
  end: number;

  results: any[];
  query: any[];
  displayResults: any[];
  displayedColumns: string[];
  avalaibleColumns: string[];

  textToSave = JSON.stringify(RESULTS);

  constructor(
    private executeRequestService: ExecuteRequestService,
    private bottomSheet: MatBottomSheet,
    private router: Router,
    public sessionService: SessionService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getResults();
    if (!this.results) {
      this.router.navigate(['/form']);
      return;
    }
    this.avalaibleColumns = (this.results.length > 0) ? Object.keys(this.results[0]) : [];
    this.displayedColumns = [];
    this.avalaibleColumns.forEach(col => this.displayedColumns.push(col));
    this.sortOrder = { active: '', direction: '' };
    this.start = 0;
    this.end = this.pageSize;
    this.displayResults = this.getPageResults(this.results);
  }

  getPageResults(res: any[]): any[] {
    return (res.length > 0) ? res.slice(this.start, this.end) : [];
  }

  getSortedPageResults(col: string, dir: string): any[] {
    const res = this.results.slice();
    if (dir === 'asc') {
      res.sort((a, b) => (a[col] > b[col]) ? 1 : -1);
    } else {
      res.sort((a, b) => (a[col] < b[col] || a[col] === b[col]) ? 1 : -1);
    }
    return this.getPageResults(res);
  }

  sortData(sort: Sort) {
    this.sortOrder = sort;
    if (sort.direction !== '') {
      this.displayResults = this.getSortedPageResults(sort.active, sort.direction);
    } else {
      this.displayResults = this.getPageResults(this.results);
    }
  }

  getResults(): void {
    this.results = this.executeRequestService.getResults();
    this.query = this.executeRequestService.getQuery();
  }

  pageEvent(event: any): void {
    this.pageSize = event.pageSize;
    this.start = event.pageSize * event.pageIndex;
    this.end = this.start + event.pageSize;
    this.sortData(this.sortOrder);
    this.table.renderRows();
  }

  chooseDownloadFormat(): void {
    this.bottomSheet.open(SpoqFormatOptionsComponent, { data: this.results });
  }

  publishTool(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    // dialogConfig.height = '55vh';
    dialogConfig.width = '40vw';
    dialogConfig.data = {};
    dialogConfig.data.results = this.results;
    dialogConfig.data.query = this.query;
    const dialogRef = this.dialog.open(PublishDataverseComponent, dialogConfig);
  }

  updatePublishTool(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    // dialogConfig.height = '55vh';
    dialogConfig.width = '40vw';
    dialogConfig.data = {};
    dialogConfig.data.results = this.results;
    dialogConfig.data.query = this.query;
    dialogConfig.data.dataverse = this.sessionService.dataverset;

    const dialogRef = this.dialog.open(PublishDataverseComponent, dialogConfig);
  }
}
