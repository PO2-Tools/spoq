import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqResultsPageComponent } from './spoq-results-page.component';

describe('SpoqResultsPageComponent', () => {
  let component: SpoqResultsPageComponent;
  let fixture: ComponentFixture<SpoqResultsPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqResultsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqResultsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
