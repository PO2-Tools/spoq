import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UnitToolComponent } from './unit-tool.component';

describe('UnitToolComponent', () => {
  let component: UnitToolComponent;
  let fixture: ComponentFixture<UnitToolComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitToolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
