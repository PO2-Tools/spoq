import { Component, OnInit } from '@angular/core';
import {InitDataService} from '../services/init-data.service';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {Unit} from '../model/Unit';

@Component({
  selector: 'app-unit-tool',
  templateUrl: './unit-tool.component.html',
  styleUrls: ['./unit-tool.component.css']
})
export class UnitToolComponent implements OnInit {
  badCode: boolean;
  prettyUnit: string;
  codeUcum = '';
  codeAuto: string;
  myControl = new FormControl();
  filteredUnit: Observable<Unit[]>;

  constructor(private initDataService: InitDataService) {
    this.filteredUnit = this.myControl.valueChanges.pipe(
        startWith(''),
        map(value => {
          return value ? this._filter(value) : this.initDataService.allUnits.slice();
          // return this._filter(value);
        }));
  }

  ngOnInit() {
    this.badCode = false;
    this.prettyUnit = '';
  }

  getOptionText(option) {
    return option.code;
  }
  parseUnit() {
    this.initDataService.getPrettyPrintUnit(this.codeUcum).subscribe(value => {
      if (value) {
        this.prettyUnit = value;
        this.badCode = false;
      } else {
        this.prettyUnit = '';
        this.badCode = true;
      }
    });
  }

  private _filter(value: string): Unit[] {
    console.log('filter ; ' + value);
    let filterValue = '';
    if (value) {
      filterValue = value.toLowerCase();
    }
    return this.initDataService.allUnits.filter(value1 => value1.group.toLowerCase().indexOf(filterValue) >= 0);
  }

  _insert() {
    this.codeUcum += this.codeAuto;
    this.parseUnit();
  }
}
