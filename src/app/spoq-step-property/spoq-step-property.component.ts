import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Property } from '../model/Property';
import { Step } from '../model/Step';
import { InitDataService } from '../services/init-data.service';
import {Observation} from '../model/Observation';
import {SessionService} from '../services/session.service';
import {Process} from '../model/Process';

@Component({
  selector: 'app-spoq-step-property',
  templateUrl: './spoq-step-property.component.html',
  styleUrls: ['./spoq-step-property.component.css']
})
export class SpoqStepPropertyComponent implements OnInit {
  @Input() property: Property;
  @Input() selectedObservation: Observation;

  @Output() close = new EventEmitter<boolean>();

  propertyFormData: Property;
  showValue: boolean;
  showObject: boolean;

  constructor(public initDataService: InitDataService) { }

  ngOnInit() {
    this.showValue = false;
    this.showObject = false;
    if (this.property.name) {
      // this.propertyFormData = new Property(this.property.name, this.property.uri);
      // this.propertyFormData.availableUnits = this.property.availableUnits;
      // this.propertyFormData.units = this.property.uri;
      // this.propertyFormData.availableObjects = this.property.availableObjects;
      // this.propertyFormData.includeObjects = this.property.includeObjects;
      // this.propertyFormData.excludeObjects = this.property.excludeObjects;
      // this.initDataService.getStepObservationProperty(this.selectedProcess, this.selectedStep, this.selectedObservation)
      //     .subscribe(prop => {
      //   prop.forEach(p => {
      //     if (p.uri === this.property.uri) {
      //       this.propertyFormData = p;
      //     }
      //   });
      // });
      this.propertyFormData =  {...this.property} as Property;

      // tslint:disable-next-line:max-line-length
      // this.initDataService.getPropertyData(this.selectedProcessUri, this.selectedStepUri, this.selectedObservation.uri, this.propertyFormData).
      // subscribe(prop => {
      //   this.propertyFormData = prop;
      //   if (!this.property.units) { this.property.units = prop.availableUnits[0]; }
      // });
    }
    if (this.property.numValue) {
      this.showValue = true;
    }
  }

  closeCard(): void {
    this.close.emit(true);
  }

  propertySelected(selectedProperty: Property): void {
    console.log('property selected : ' + selectedProperty);
    // this.property.uri = selectedProperty.uri;
    // this.property.name = selectedProperty.name;
    // this.property.units = null;
    // this.property.availableUnits = [];
    // // this.property.observationMaterials = [null];
    // this.property.availableObjects = [];
    // this.property.includeObjects = [];
    // this.property.excludeObjects = [];

    // this.initDataService.getPropertyData(this.selectedProcessUri, this.selectedStepUri, this.selectedObservation.uri, selectedProperty).
    // subscribe(prop => {
    //   this.property.units = prop.availableUnits[0];
    //   this.property.availableUnits = prop.availableUnits;
    //   this.property.availableObjects = prop.availableObjects;
    //   this.property.includeObjects = prop.includeObjects;
    //   this.property.excludeObjects = prop.excludeObjects;
    //   this.propertyFormData = prop;
    // });
    // this.property = JSON.parse(JSON.stringify(selectedProperty)) as Property;
    Object.assign(this.property, {...selectedProperty} as Property);
    this.propertyFormData = this.property;
  }

  closeValue(): void {
    this.property.boundary = null;
    this.property.numValue = null;
  }

  valueTyped(): void {
    if (!this.property.boundary) {
      this.property.boundary = 'exact';
    }
    if (!this.property.units) {
      this.property.units = this.propertyFormData.availableUnits[0];
    }
  }
}
