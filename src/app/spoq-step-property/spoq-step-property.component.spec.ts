import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqStepPropertyComponent } from './spoq-step-property.component';

describe('SpoqStepPropertyComponent', () => {
  let component: SpoqStepPropertyComponent;
  let fixture: ComponentFixture<SpoqStepPropertyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqStepPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqStepPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
