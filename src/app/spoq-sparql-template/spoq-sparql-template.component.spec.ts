import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqSparqlTemplateComponent } from './spoq-sparql-template.component';

describe('SpoqSparqlTemplateComponent', () => {
  let component: SpoqSparqlTemplateComponent;
  let fixture: ComponentFixture<SpoqSparqlTemplateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqSparqlTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqSparqlTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
