import { Component, OnInit, Input, DoCheck } from '@angular/core';
import { SparqlTemplate } from '../model/SparqlTemplate';
import { SparqlEditorService } from '../services/sparql-editor.service';
import { FormParameters } from '../model/FormParameters';

@Component({
  selector: 'app-spoq-sparql-template',
  templateUrl: './spoq-sparql-template.component.html',
  styleUrls: ['./spoq-sparql-template.component.css']
})
export class SpoqSparqlTemplateComponent implements OnInit, DoCheck {
  @Input() form: FormParameters;
  previousForm: string;

  templatesList: SparqlTemplate[];

  constructor(private sparqlEditorService: SparqlEditorService) { }

  ngOnInit() {
    this.generateTemplatesList();
    this.previousForm = '';
  }

  ngDoCheck() {
    if (!this.form) { return; }
    if (this.previousForm === JSON.stringify(this.form)) { return; }
    this.previousForm = JSON.stringify(this.form);
    this.generateTemplatesList();
  }

  async generateTemplatesList() {
    this.templatesList = await this.sparqlEditorService.generateTemplates();
  }

}
