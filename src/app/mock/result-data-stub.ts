export const RESULTS: any = [
    {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FF',
        result1_label: 'bulk density [[ 1.516e+00 ; 1.400e-02 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.294e+01 ; 2.700e-01 ]] %',
        interest1_label: 'BCER-FF',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_762715836'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }, {
        process_label: 'Biocomposite Process from Vine shoot+PHBV',
        step1_label: 'Vine shoot fibers - sieving sieving size 0.2 mm',
        obs1_label: 'observations BCER-FG',
        result1_label: 'bulk density [[ 1.487e+00 ; 5.000e-03 ]] g/cm3',
        result2_label: 'Cellulose rate [[ 3.404e+01 ; 2.080e+00 ]] %',
        interest1_label: 'BCER-FG',
        prod1: 'domain:data/Malice_data#po2_component_Vine_shoot_fiber_736540612'
    }
]