import { SparqlTemplate } from '../model/SparqlTemplate';

export const TEMPLATE_STUB: SparqlTemplate[] = [
    {
        title: '',
        sparql:
            'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n' +
            'PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>\n' +
            'PREFIX PO2-Domain: <http://opendata.inrae.fr/PO2/Ontology/PO2_biorefinery/>\n' +
            'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n' +
            'PREFIX time: <http://www.w3.org/2006/time#>\n' +
            '\n' +
            'SELECT  ?subclass ?directSuperClass ?subclasslabel ?dbxref ?otherlabel ?otherclass\n' +
            'WHERE\n' +
            '{\n' +
            '[...]\n' +
            '}\n',
        subTemplates: null
    }, {
        title: '###### Process p1 ######',
        sparql:
            '?process rdfs:subClassOf PO2:Transformation_Process.\n' +
            '?process rdfs:label ?process_label.\n' +
            '?process rdfs:label "p1".\n',
        subTemplates: null
    }, {
        title: '###### Step s1 ######',
        sparql:
            '?iti1 PO2:hasForStep ?step1.\n' +
            '?step1 rdfs:label "Vine shoot fibers - sieving sieving size 0.2 mm". \n' +
            '?step1 rdfs:label ?step1_label.\n',
        subTemplates: [{
            title: '###### M&M Step s1 ######',
            sparql:
                '?step1 <http://www.w3.org/ns/sosa/madeByActuator> ?mat1.\n' +
                '?mat1 rdfs:label "sieves". \n' +
                '?mat1 rdfs:label ?mat1_label.\n',
            subTemplates: [{
                title: '###### Attribut du Materiel ######',
                sparql:
                    '?mat1 PO2:hasSettingMeasure ?result_mat1.\n' +
                    '?result_mat1 <http://www.w3.org/ns/ssn/hasProperty> ?attr_mat1.\n' +
                    '?attr_mat1 rdf:type <http://opendata.inrae.fr/PO2/Ontology/PO2_biorefinery/attribute/Sieving_size>.\n',
                subTemplates: null
            }]
        }, {
            title: '###### Output Step s1 ######',
            sparql:
                '?step1 PO2:hasOutput ?compo1.\n' +
                '?compo1 rdf:type <http://opendata.inrae.fr/PO2/Ontology/PO2_biorefinery/component/Vine_shoot_fiber>.\n' +
                '?compo1 PO2:isComposedOf ?prod1.\n' +
                '?prod1 rdf:type <http://opendata.inrae.fr/PO2/Ontology/PO2_biorefinery/component/Vine_shoot_fiber>.\n',
            subTemplates: null
        }]
    },
]
