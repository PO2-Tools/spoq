import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqSparqlTemplateCardComponent } from './spoq-sparql-template-card.component';

describe('SpoqSparqlTemplateCardComponent', () => {
  let component: SpoqSparqlTemplateCardComponent;
  let fixture: ComponentFixture<SpoqSparqlTemplateCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqSparqlTemplateCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqSparqlTemplateCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
