import { Component, OnInit, Input } from '@angular/core';
import { SparqlTemplate } from '../model/SparqlTemplate';
import { MatDialog } from '@angular/material/dialog';

import { SpoqSparqlTemplateCardDialogComponent } from '../spoq-sparql-template-card-dialog/spoq-sparql-template-card-dialog.component';

@Component({
  selector: 'app-spoq-sparql-template-card',
  templateUrl: './spoq-sparql-template-card.component.html',
  styleUrls: ['./spoq-sparql-template-card.component.css']
})
export class SpoqSparqlTemplateCardComponent implements OnInit {
  @Input() templatesList: SparqlTemplate[];
  @Input() level: number;


  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }


  openSaveDialog(text: string): void {
    const dialogRef = this.dialog.open(SpoqSparqlTemplateCardDialogComponent, {
      data: { text }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
