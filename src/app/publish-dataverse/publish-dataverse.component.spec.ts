import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PublishDataverseComponent } from './publish-dataverse.component';

describe('PublishDataverseComponent', () => {
  let component: PublishDataverseComponent;
  let fixture: ComponentFixture<PublishDataverseComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishDataverseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishDataverseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
