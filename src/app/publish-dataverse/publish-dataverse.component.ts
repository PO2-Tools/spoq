import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {SessionService} from '../services/session.service';
import {LoginService} from '../services/login.service';
import {Dataverset} from '../model/Dataverset';
import {InitDataService} from '../services/init-data.service';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {ResultsFormatConverterService} from '../services/results-format-converter.service';
import {EditorSave} from '../model/EditorSave';
import {FormSave} from '../model/FormSave';
import {WholeSave} from '../model/WholeSave';



@Component({
  selector: 'app-publish-dataverse',
  templateUrl: './publish-dataverse.component.html',
  styleUrls: ['./publish-dataverse.component.css']
})
export class PublishDataverseComponent implements OnInit {



  title: string;
  description: string;
  landing: string;
  authors: string;
  subjects = new FormControl();
  myControl = new FormControl();
  keywords: string; // comma separated
  queryPrefix: string;
  allSubjects = ['Agricultural Sciences', 'Arts and Humanities', 'Astronomy and Astrophysics', 'Business and Management', 'Chemistry',
    'Computer and Information Science', 'Earth and Environmental Sciences', 'Engineering', 'Law', 'Mathematical Sciences',
    'Medicine, Health and Life Sciences', 'Physics', 'Social Sciences', 'Other'];
  allQuery = [];
  private datasetID: string;
  private datasetDOI: string;

  constructor(private logginService: LoginService,
              private initService: InitDataService,
              private sessionService: SessionService,
              private snackBar: MatSnackBar,
              private dialogRef: MatDialogRef<PublishDataverseComponent>,
              private resultFormatConverter: ResultsFormatConverterService,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.landing = 'http://quantum.agroparistech.fr/spoq';
    this.authors = this.logginService.getcurrentUser().username;
    if (this.data.dataverse) {
      // on es sur une mise a jour
      const dataverse: Dataverset = this.data.dataverse;
      this.title = dataverse.title;
      this.description = dataverse.description;
      this.landing = dataverse.landingpage;
      this.authors = dataverse.authors;
      this.keywords = dataverse.keywords;
      this.subjects.patchValue( dataverse.subjects.split(','));
      this.datasetID = dataverse.datasetID;
      this.datasetDOI = dataverse.doi;
      this.queryPrefix = '';
      this.allQuery = dataverse.allQuery;
    }
  }

  blobToFile = (theBlob: Blob, fileName: string): File => {
    const b: any = theBlob;
    b.lastModifiedDate = new Date();
    b.name = fileName;
    return theBlob as File;
  }

  publish(update: boolean): void {
    this.sessionService.datasetsLoading.push('publish');

    const editorSave = new EditorSave();
    editorSave.name = this.title;
    editorSave.selectedDatasetsForEditor = this.sessionService.selectedEditorDatasets;
    editorSave.editorSave = this.sessionService.editorContent;

    const formSave = new FormSave();
    formSave.name = this.title;
    formSave.save = this.sessionService.selectedParameters;

    const wholeSave = new WholeSave();
    wholeSave.editorSave = editorSave;
    wholeSave.formSave = formSave;
    wholeSave.name = this.title;


    const newDataset = new Dataverset(this.title, this.description, this.authors, this.keywords,
        this.subjects.value.toString(), this.landing, '', '', wholeSave, this.queryPrefix);
    if (update) {
      newDataset.datasetID = this.datasetID;
      newDataset.doi = this.datasetDOI;
    }
    const fjson = new Blob([JSON.stringify(this.data.results)], { type: 'text/plain'});
    newDataset.addFile(this.blobToFile(fjson, 'raw_result.json'));

    const stringTSV = this.resultFormatConverter.convertToTSV(this.data.results);
    const fTSV = new Blob([stringTSV], { type: 'text/plain'});
    newDataset.addFile(this.blobToFile(fTSV, 'raw_result.tsv'));

    const qjson = new Blob([JSON.stringify(newDataset.save)], { type: 'text/plain'});
    newDataset.addFile(this.blobToFile(qjson, 'query.json'));


    this.initService.publishToDataverse(newDataset, update).subscribe(value => {
      this.sessionService.datasetsLoading.splice(0, 1);
      if (value) {
       console.log('ok');
       this.dialogRef.close();
       this.snackBar.open('Dataset published.' + value, 'Ok', {
          duration: 0,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
      } else {
        this.snackBar.open('An error occurred.', 'Ok', {
          duration: 0,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
      }
    });
  }

}
