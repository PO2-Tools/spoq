import { Component, OnInit } from '@angular/core';
import { SparqlEditorService } from '../services/sparql-editor.service';
import { ExecuteRequestService } from '../services/execute-request.service';
import { Router } from '@angular/router';
import { SessionService } from '../services/session.service';
import { Dataset } from '../model/Dataset';
import { InitDataService } from '../services/init-data.service';
import 'codemirror/mode/sparql/sparql';
import 'codemirror/mode/turtle/turtle';
import 'codemirror/addon/edit/matchbrackets';
import 'codemirror/addon/edit/closebrackets';
@Component({
  selector: 'app-spoq-sparql-page',
  templateUrl: './spoq-sparql-page.component.html',
  styleUrls: ['./spoq-sparql-page.component.css']
})
export class SpoqSparqlPageComponent implements OnInit {
  datasets: Dataset[];

  formIsEmpty: boolean;
  showError: boolean;
  errorMessage: string;
  showOption: boolean;

  previouslySelectedDatasets: Dataset[];

  constructor(
    private editorService: SparqlEditorService,
    private executeRequestService: ExecuteRequestService,
    private router: Router,
    public sessionService: SessionService,
    private initDataService: InitDataService
  ) { }

  ngOnInit() {
    this.sessionService.advancedUser = true;
    this.formIsEmpty = this.sessionService.hasFormBeenFilled();
    this.initDataService.getInitData().subscribe(data => this.datasets = data);
    this.resetErrorMessage();
  }

  test() {
    console.log({
      selectedEditorDatasets: this.sessionService.selectedEditorDatasets, editorContent: this.sessionService.editorContent,
      repoUrlSparqlEditor: this.sessionService.repoUrlSparqlEditor
    });
  }

  async resetCopySparql() {
    this.cacheSelectedRepos();
    if (this.sessionService.editorContent) {
      if (!confirm('This action will replace all of the editor content')) { return; }
    }
    this.sessionService.editorContent = await this.editorService.getFromForm();
    this.sessionService.selectedEditorDatasets = this.sessionService.selectedParameters.selectedDatasets.concat([]);
    this.datasetSetChange();
    this.resetErrorMessage();
    console.log('Form copied');
  }

  execSparql(): void {
    console.log('SPARQL request executed');
    this.sessionService.executing = true;
    this.executeRequestService.executeEditor(this.sessionService.editorContent).subscribe(errorMessage => {
      this.sessionService.executing = false;
      if (errorMessage === '') {
        this.router.navigate(['/results']);
      } else {
        this.setErrorMessage(errorMessage);
      }
    });
  }

  datasetSetChange() {
    if (!this.selectedNewRepos()) { return; }
    this.initDataService.updateRepositoryAdress(this.sessionService.selectedEditorDatasets, false).subscribe(isUpdated => {
      console.log('repository location is update : ' + isUpdated);
    })  ;
  }

  loadEditor(success: boolean): void {
    if (success) {
      this.resetErrorMessage();
    } else {
      alert('Error while loading selected save, please try again');
    }
  }

  resetErrorMessage() {
    this.showError = false;
    this.errorMessage = '';
  }

  setErrorMessage(message: string) {
    this.showError = true;
    this.errorMessage = message;
  }

  compareObjects(o1: any, o2: any): boolean {
    if (o1 == null || o2 == null) {
      return false;
    }
    return o1.name === o2.name;
  }

  cacheSelectedRepos(): void {
    console.log('caching');
    this.previouslySelectedDatasets = this.sessionService.selectedEditorDatasets.concat([]);
  }

  selectedNewRepos(): boolean {
    console.log('selectedNewRepos?');
    console.log('this.sessionService.selectedEditorDatasets.length !== this.previouslySelectedDatasets.length?');
    console.log(this.sessionService.selectedEditorDatasets.length);
    console.log(this.previouslySelectedDatasets.length);
    if (this.sessionService.selectedEditorDatasets.length !== this.previouslySelectedDatasets.length) {
      console.log('this.sessionService.selectedEditorDatasets.length !== this.previouslySelectedDatasets.length YES');
      return true;
    }
    console.log('no, same size');
    console.log(this.sessionService.selectedEditorDatasets);
    console.log(this.previouslySelectedDatasets);
    for (const newDts of this.sessionService.selectedEditorDatasets) {
      if (this.previouslySelectedDatasets.findIndex(oldDts => oldDts.name === newDts.name) === -1) {
        return true;
      }
    }
    return false;
  }
}
