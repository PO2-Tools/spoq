import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqSparqlPageComponent } from './spoq-sparql-page.component';

describe('SpoqSparqlPageComponent', () => {
  let component: SpoqSparqlPageComponent;
  let fixture: ComponentFixture<SpoqSparqlPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqSparqlPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqSparqlPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
