import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqLoadingBarComponent } from './spoq-loading-bar.component';

describe('SpoqLoadingBarComponent', () => {
  let component: SpoqLoadingBarComponent;
  let fixture: ComponentFixture<SpoqLoadingBarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqLoadingBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqLoadingBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
