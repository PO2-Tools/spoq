import {Component, OnInit, Inject, Output, EventEmitter} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {SaveLoadService} from '../services/save-load.service';
import {Router} from '@angular/router';
import {EditorSave} from '../model/EditorSave';
import {FormSave} from '../model/FormSave';

@Component({
  selector: 'app-spoq-load-dialog',
  templateUrl: './spoq-load-dialog.component.html',
  styleUrls: ['./spoq-load-dialog.component.css']
})
export class SpoqLoadDialogComponent implements OnInit {

  @Output() load = new EventEmitter<boolean>();

  save: string;
  type: string;
  saveDescription: string;
  loadError: boolean;


  constructor(private saveService: SaveLoadService,
              private router: Router,
              public dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  updateDescription(desc: any) {
    this.saveDescription = desc.saveDesc;
  }

  startLoading(result: any): void {
    this.loadError = false;
    if (result.type === 'form') {
      const saveJSON = JSON.parse(result.save.save);
      const formSave = new FormSave();
      formSave.name = result.save.saveName;
      formSave.save = saveJSON;
      this.saveService.loadFormState(formSave).subscribe(response => {
        this.load.emit(response);
        if (response) {
          this.router.navigate(['/form']);
          this.dialog.closeAll();
        } else {
          this.loadError = true;
        }
      });
    } else if (result.type === 'sparql') {
      const saveJSON = JSON.parse(result.save.save);
      const editorSave = new EditorSave();
      editorSave.name = saveJSON.name;
      editorSave.selectedDatasetsForEditor = saveJSON.selectedDatasetsForEditor;
      editorSave.editorSave = saveJSON.editorSave;
      this.saveService.loadEditorState(editorSave).subscribe(response => {
        this.load.emit(response);
        if (response) {
          this.router.navigate(['/sparql']);
          this.dialog.closeAll();
        } else {
          this.loadError = true;
        }
      });
    } else {
      this.saveService.loadWholeState(result.save).subscribe(response => {
          this.load.emit(response);
          if (response) {
            this.dialog.closeAll();
          } else {
            this.loadError = true;
          }
        }
      );
    }
  }
}
