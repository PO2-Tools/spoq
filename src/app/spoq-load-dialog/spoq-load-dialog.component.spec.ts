import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqLoadDialogComponent } from './spoq-load-dialog.component';

describe('SpoqLoadDialogComponent', () => {
  let component: SpoqLoadDialogComponent;
  let fixture: ComponentFixture<SpoqLoadDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqLoadDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqLoadDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
