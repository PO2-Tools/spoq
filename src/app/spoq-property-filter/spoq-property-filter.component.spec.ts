import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqPropertyFilterComponent } from './spoq-property-filter.component';

describe('SpoqPropertyFilterComponent', () => {
  let component: SpoqPropertyFilterComponent;
  let fixture: ComponentFixture<SpoqPropertyFilterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqPropertyFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqPropertyFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
