import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ObservationMaterial } from '../model/ObservationMaterial';
import { InitDataService } from '../services/init-data.service';
import {ObjectOI} from '../model/ObjectOI';

@Component({
  selector: 'app-spoq-property-filter',
  templateUrl: './spoq-property-filter.component.html',
  styleUrls: ['./spoq-property-filter.component.css']
})
export class SpoqPropertyFilterComponent implements OnInit {
  @Input() selectedProcessUri: string;
  @Input() selectedStepUri: string;

  // @Input() selectedObsMaterial: ObservationMaterial;
  // @Input() obsMaterialFormData: ObservationMaterial[];
  // @Output() selectedObsMaterialChange = new EventEmitter<ObservationMaterial>();


  @Input() selectedObsOOI: ObjectOI[];
  @Input() obsOOIFormData: ObjectOI[];
  @Output() selectedObsOOIChange = new EventEmitter<ObjectOI[]>();

  // selectedFormObsMaterial: ObservationMaterial;
  selectedFormObsOOI: ObjectOI;

  // doNotFilterObsMat: ObservationMaterial;
  doNotFilterObsOOI: ObjectOI;

  constructor(public initDataService: InitDataService) { }

  test(): void {
    console.log(this.selectedObsOOI);
  }

  ngOnInit() {
    // this.doNotFilterObsMat = new ObservationMaterial('doNotFilter', '');
    // if (this.selectedObsMaterial) {
    //   this.selectedFormObsMaterial = new ObservationMaterial(this.selectedObsMaterial.name, this.selectedObsMaterial.uri);
    //   // Not implemented yet, do not use
    //   /*this.initDataService.getObsMaterialParams(this.selectedProcessUri, this.selectedStepUri, this.selectedFormObsMaterial)
    //     .subscribe(params =>
    //       this.selectedFormObsMaterial.obsContParam = params
    //     );*/
    // }
    this.doNotFilterObsOOI = new ObjectOI('doNotFilter', '');
    if (this.selectedObsOOI && this.selectedObsOOI.length === 1 && this.selectedObsOOI[0]) {
      this.selectedFormObsOOI = new ObjectOI(this.selectedObsOOI[0].name, this.selectedObsOOI[0].uri);
    }
  }

  // compareObjects(o1: any, o2: any): boolean {
  //   if (o1 == null || o2 == null) {
  //     return false;
  //   }
  //   return o1.name === o2.name;
  // }

  compareOOI(o1: ObjectOI, o2: ObjectOI): boolean {
    if (o1 == null || o2 == null) {
      return false;
    }
    return o1.uri === o2.uri;
  }

  obsOOISelected(ooi: ObjectOI): void {
    this.selectedFormObsOOI = ooi;
    if (ooi == null) {
      this.selectedObsOOI = [];
      this.selectedObsOOIChange.emit(this.selectedObsOOI);
      return;
    }
    /*to remove when multiple OOI selection is done*/
    this.selectedObsOOI = [];

    this.selectedObsOOI.push(new ObjectOI(this.selectedFormObsOOI.name, this.selectedFormObsOOI.uri));
    this.selectedObsOOIChange.emit(this.selectedObsOOI);
    console.log(this.selectedObsOOI);
  }

  // obsMaterialSelected(component: ObservationMaterial): void {
  //   this.selectedFormObsMaterial = component;
  //   if (component == null) {
  //     this.selectedObsMaterial = null;
  //     this.selectedObsMaterialChange.emit(this.selectedObsMaterial);
  //     return;
  //   }
  //   this.selectedObsMaterial = new ObservationMaterial(component.name, component.uri);
  //   this.selectedObsMaterial.obsContParam.push(null);
  //   this.selectedObsMaterialChange.emit(this.selectedObsMaterial);
  //   // Not implemented yet, do not use
  //   /*this.initDataService.getObsMaterialParams(this.selectedProcessUri, this.selectedStepUri, component).subscribe(params =>
  //     component.obsContParam = params
  //   );*/
  //   console.log(this.selectedObsMaterial);
  // }

}
