import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-spoq-sparql-error-message',
  templateUrl: './spoq-sparql-error-message.component.html',
  styleUrls: ['./spoq-sparql-error-message.component.css']
})
export class SpoqSparqlErrorMessageComponent implements OnInit {
  @Input() errorMessage: string;

  constructor() { }

  ngOnInit() {
  }

}
