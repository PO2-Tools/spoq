import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqSparqlErrorMessageComponent } from './spoq-sparql-error-message.component';

describe('SpoqSparqlErrorMessageComponent', () => {
  let component: SpoqSparqlErrorMessageComponent;
  let fixture: ComponentFixture<SpoqSparqlErrorMessageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqSparqlErrorMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqSparqlErrorMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
