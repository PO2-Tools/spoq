import { TestBed } from '@angular/core/testing';

import { ExecuteRequestService } from './execute-request.service';

describe('ExecuteRequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExecuteRequestService = TestBed.get(ExecuteRequestService);
    expect(service).toBeTruthy();
  });
});
