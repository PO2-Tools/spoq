import { Injectable } from '@angular/core';

const TAB_SEP = '	';
const LINE_BREAK = '\n';

@Injectable({
  providedIn: 'root'
})
export class ResultsFormatConverterService {

  constructor() { }

  convertToTSV(data: any[]): string {
    let res = '';
    const keys = Object.keys(data[0]);

    for (const col of keys) {
      res += col;
      if (col !== keys[keys.length - 1]) {
        res += TAB_SEP;
      }
    }
    res += LINE_BREAK;
    for (const line of data) {
      for (const col of keys) {
        res += line[col];
        if (col !== keys[keys.length - 1]) {
          res += TAB_SEP;
        }
      }
      res += LINE_BREAK;
    }

    return res;
  }
}

