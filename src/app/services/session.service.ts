import { Injectable } from '@angular/core';
import { Dataset } from '../model/Dataset';
import { Process } from '../model/Process';
import { FormParameters } from '../model/FormParameters';
import { LoginService } from './login.service';
import { Observable } from 'rxjs';
import { UserService } from './user.service';
import {Dataverset} from '../model/Dataverset';
import {Observation} from '../model/Observation';


@Injectable({
  providedIn: 'root'
})
export class SessionService {
  language: string;

  advancedUser: boolean;
  loggedIn: boolean;

  repoUrl: string;
  repoUrlSparqlEditor: string;

  selectedParameters: FormParameters;
  editorContent: string;
  selectedEditorDatasets: Dataset[];
  maxNbLinesEditor: number;

  /* Liste des dataset disponible */
  availableDatasets: Dataset[];
  /* Liste des process disponible*/
  availableProcessTree: Process[];



  doNotRemind: boolean;
  doNotFilterProcess: Process = new Process('doNotFilter', 'http://opendata.inrae.fr/PO2/core/Process', '', '');
  doNotFilterObservation: Observation = new Observation('doNotFilter', 'http://opendata.inrae.fr/PO2/core/Observation');


  isTemplateCardShown: boolean;

  stepTypes: string[];

  results: any[];
  query: any[];

  executing: boolean;
  datasetsLoading: string[];
  justLoaded: boolean[];
  dataverset: Dataverset;

  constructor(
    private loginService: LoginService,
    private userService: UserService
  ) {
    this.language = 'en'; // To be set later

    userService.checkIfLoggedIn().subscribe(res => this.loggedIn = res);
    this.reset();
    this.advancedUser = false;
    this.executing = false;
    this.datasetsLoading = [];
    this.justLoaded = [];
    this.doNotRemind = false;
    this.maxNbLinesEditor = 1000;
    this.dataverset = null;
  }

  reset(): void {
    // this.doNotFilterProcess = this.getDoNotFilterProcess();
    this.selectedParameters = new FormParameters();
    this.selectedEditorDatasets = [];
    this.availableDatasets = [];
    this.availableProcessTree = [];
    this.stepTypes = [];
    this.results = [];
    this.query = [];
    this.dataverset = null;
  }

  // getDoNotFilterProcess(): Process {
  //   return new Process('doNotFilter', 'http://opendata.inrae.fr/PO2/core/Transformation_Process', '', '');
  // }

  setResults(res: any[]): void {
    this.results = res;
    this.executing = false;
  }

  setQuery(que: any[]): void {
    this.query = que;
  }

  loadDataverset(dataset: Dataverset): void {
    this.dataverset = dataset;
  }

  clearDataverset(): void {
    this.dataverset = null;
  }

  isAdvancedUser() {
    return this.advancedUser;
  }

  switchMode() {
    this.advancedUser = !this.advancedUser;
    this.showHideTemplates();
  }

  showHideTemplates() {
    this.isTemplateCardShown = this.advancedUser;
  }

  loadCachedFormSelection(): FormParameters {
    return this.selectedParameters;
  }

  isCached(): boolean {
    if (this.selectedParameters.selectedDatasets.length !== 0) { return true; }
    if (this.selectedParameters.selectedProcess) { return true; }
    return this.selectedParameters.selectedSteps.length !== 0;
  }

  hasFormBeenFilled(): boolean {
    return this.isCached() && this.selectedParameters.selectedDatasets.length !== 0;
  }

  login(usr: string, pwd: string): Observable<boolean> {
    return this.loginService.login(usr, pwd);
  }

  logout(): Observable<boolean> {
    return this.loginService.logout();
  }
}
