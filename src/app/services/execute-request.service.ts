import {Injectable} from '@angular/core';
import {FormParameters} from '../model/FormParameters';
import {Observable} from 'rxjs';
import {SessionService} from './session.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {SparqlEditorService} from './sparql-editor.service';
import {LoginService} from './login.service';

@Injectable({
  providedIn: 'root'
})
export class ExecuteRequestService {
  readonly options = {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  };

  constructor(
    private http: HttpClient,
    private sessionService: SessionService,
    private sparqlService: SparqlEditorService,
    private loginService: LoginService
  ) {
  }


  async executeForm(params: FormParameters): Promise<Observable<boolean>> {
    const url: string = this.sessionService.repoUrl;
    const req: string = await this.sparqlService.getSparqlQuery();
    const que: any = {};
    que.query = req;
    que.limit = '1000';
    if (this.sessionService.loggedIn) {
      que.user = this.loginService.getcurrentUser().username;
    } else {
      que.user = 'anonymous';
    }
    que.date = new Date();
    que.repositories = this.sessionService.selectedParameters.selectedDatasets.map(d => d.name).join(',');
    this.sessionService.setQuery(que);
    // url = url + '?limit=1000&query=' + encodeURIComponent(req);
    const body = `limit=1000&query=` + encodeURIComponent(req);

    return this.http.post<any>(url, body, this.options).pipe(
      catchError(error => {
        console.error('An error occurred, contact an admin and send them the following report: ');
        console.error(error.error);
        console.error(req);
        console.error('END OF REPORT');
        return [error.error];
      }),
      map(res => {
        if (res.head) {
          this.sessionService.setResults(this.convertResponseJSON(res));
          return true;
        }
        return false;
      }));
  }

  executeEditor(req: string): Observable<string> {
    const url: string = this.sessionService.repoUrlSparqlEditor;
    const body = `limit=` + this.sessionService.maxNbLinesEditor + `&query=` + encodeURIComponent(req);

    const que: any = {};
    que.query = req;
    que.limit = this.sessionService.maxNbLinesEditor;
    if (this.sessionService.loggedIn) {
      que.user = this.loginService.getcurrentUser().username;
    } else {
      que.user = 'anonymous';
    }
    que.date = new Date();
    que.repositories = this.sessionService.selectedParameters.selectedDatasets.map(d => d.name).join(',');
    this.sessionService.setQuery(que);

    return this.http.post<any>(url, body, this.options).pipe(
      catchError(error => {
        return [error.error];
      }),
      map(res => {
        if (res.results && res.results.bindings.length === 0) {
          return 'No data matching current query available in table'; // GraphDB -> 'No data available in table'
        }
        if (res.head) {
          this.sessionService.setResults(this.convertResponseJSON(res));
          return '';
        }
        return res;
      }));
  }

  convertResponseJSON(res: any): any[] {
    const columns: string[] = res.head.vars;
    const ret: any[] = [];

    res.results.bindings.forEach(binding => {
      const line = {};
      columns.forEach(col => {
        line[col] = (binding[col]) ? binding[col].value : '';
      });
      ret.push(line);
    });
    return ret;
  }

  getResults(): any {
    return this.sessionService.results;
  }

  getQuery(): any {
    return this.sessionService.query;
  }
}
