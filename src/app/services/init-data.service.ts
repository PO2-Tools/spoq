import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Dataset} from '../model/Dataset';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Process} from '../model/Process';
import {Step} from '../model/Step';
import {Filter} from '../model/Filter';
import {Measurement} from '../model/Measurement';
import {Property} from '../model/Property';
import {ObservationMaterial} from '../model/ObservationMaterial';

import {
    checkUnit,
    getAllUnits,
    getRepository,
    getSIQuantity,
    listRepository,
    local,
    publishDataverse,
    updateDataverse
} from 'src/assets/request-adresses';
import {
  DATASET_PROCESSES_QUERY,
  OBJECT_FILTERED_HIERARCHIE,
  OBJECT_FULL_HIERARCHIE,
  OBS_MAT_QUERY,
  OBS_MATERIAL_PARAMS_QUERY,
  PROCESS_STEPS_TYPES_LABEL_QUERY,
  STEP_INPUT_COMPONENT_INGREDIENTS_QUERY,
  STEP_INPUT_QUERY,
  STEP_MAT_QUERY,
  STEP_MATERIALS_PARAMS_QUERY,
  STEP_OBS_PROP_QUERY,
  STEP_OBS_TYPE_QUERY,
  STEP_OUPUT_QUERY,
  STEP_OUTPUT_COMPONENT_INGREDIENTS_QUERY,
  STEP_PROP_QUERY,
  TEMPLATE_OBJECT_HIERARCHIE
} from '../sparql-queries';
import {handleError} from '../handleError';
import {SessionService} from './session.service';
import {ObsControlParam} from '../model/ObsControlParam';
import {Observation} from '../model/Observation';
import {Unit} from '../model/Unit';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Dataverset} from '../model/Dataverset';
import {ObjectOI} from '../model/ObjectOI';


@Injectable({
    providedIn: 'root'
})
export class InitDataService {
    allUnits: Unit[];
    prettyUnit: Map<string, string>;
    readonly options = {
        headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };


    constructor(
        private http: HttpClient,
        private session: SessionService,
        private snackBar: MatSnackBar
    ) {
        this.session.repoUrl = 'not initialized';
        this.session.repoUrlSparqlEditor = 'not initialized';
        this.prettyUnit = new Map<string, string>();
        this.allUnits = [];
    }

    initAllUnitsMap() {
        this.http.post(local + getAllUnits, {})
            .pipe(catchError(handleError))
            .subscribe(value => {
                Object.keys(value).forEach(key => {
                    this.allUnits.push(new Unit(key, value[key]));
                });
            });
    }

    updateRepositoryAdress(repos: Dataset[], inForm: boolean): Observable<boolean> {
        if (repos.length === 0) {
            if (inForm) {
                this.clearReposForm();
                this.session.selectedParameters.selectedDataset = null;
            } else {
                this.clearReposSparql();
            }
            return of(true);
        } else {
            return this.getRepositoryAdress(repos).pipe(
                map(urlWrapper => {
                    console.log('updateRepositoryAdress URL is: ' + urlWrapper.location);
                    if (inForm) {
                        this.session.repoUrl = urlWrapper.location;
                        this.session.selectedParameters.selectedDataset = new Dataset('federated', false, urlWrapper.location);
                    } else {
                        this.session.repoUrlSparqlEditor = urlWrapper.location;
                    }
                    return true;
                })
            );
        }
    }

    private clearReposForm(): void {
        this.session.repoUrl = 'not initialized';
    }
    private clearReposSparql(): void {
        this.session.repoUrlSparqlEditor = 'not initialized';
    }

    getRepositoryAdress(repos: Dataset[]): Observable<any> {
        let params = '';

        let index = 0;
        repos.forEach(repo => {
            if (index === 0) {
                params += '?id=' + repo.name;
            } else {
                params += '&id=' + repo.name;
            }
            index++;
        });

        this.session.datasetsLoading.push('loading');
        return this.http.post<any>(local + getRepository + params, {}).pipe(
            catchError(error => {
                this.snackBar.open('An error occurred. Please try later', 'Ok', {
                    duration: 5000,
                    horizontalPosition: 'center',
                    verticalPosition: 'top',
                });
                this.session.datasetsLoading.splice(0, 1);
                this.session.reset();
                this.getInitData().subscribe(data => this.session.availableDatasets = data);
                return error;
            }),
            map(res => {
                this.session.datasetsLoading.splice(0, 1);
                return res;
            }));
    }

    // destroyFederatedRepositories(url: string): void {
    //   const body = 'id=' + url.split(graphDB + '/repositories/')[1];
    //   const opt = {
    //     headers: new HttpHeaders({
    //       'Content-Type': 'application/x-www-form-urlencoded'
    //     }),
    //   };
    //   this.http.post<any>(local + deleteRepository, body, opt)
    //     .subscribe(() => {
    //       console.log('Repository deleted: ' + url);
    //     });
    // }

    getInitData(): Observable<Dataset[]> {
        console.log('initdata');
        const ret = this.http.post(local + listRepository, {}).pipe(
            catchError(handleError),
            map(datasets => {
                return this.extractDatasetsName(datasets).sort(((a, b) => a.name.localeCompare(b.name)));
            }));

        return ret;
    }

    getDatasetProcessTree(processes: Process[]): Promise<Process[]> {
        return new Promise<Process[]>((resolve, reject) => {
            this.getProcessTypeHierarchy(processes).subscribe(processesH => {
                const result = this.buildProcessHierarchy(processesH);
                resolve(result);
            });
        });
    }

    getListProcesses(dataset: Dataset): Observable<Process[]> {
        console.log('get dataset process');
        // const queryAdress: string = graphDB + executeQuery.replace(':repo_ID', dataset.name);
        const queryAdress = dataset.location;
        const body = `query=` + encodeURIComponent(this.setQueryLanguage(DATASET_PROCESSES_QUERY));

        return this.http.post<Process[]>(queryAdress, body, this.options).pipe(
            catchError(handleError),
            map(processes => {
                dataset.processes = this.extractProcesses(processes).sort((a, b) => a.name.localeCompare(b.name));
                return dataset.processes;
            }));
    }

    // getDatasetsStepTypes(): Observable<Step[]> {
    //   const result: Step[] = [];
    //   const value = of(result);
    //
    //   const queryAdress: string = this.session.repoUrl;
    //   const body = `query=` + encodeURIComponent(
    //     this.setQueryLanguage(PROCESS_STEPS_TYPES_LABEL_QUERY
    //       .replace('$processURI', this.session.doNotFilterProcess.uri))
    //   );
    //
    //   this.http.post<Step[]>(queryAdress, body, this.options).pipe(
    //     catchError(handleError),
    //     map(steps => {
    //       return this.extractSteps(steps);
    //     })).subscribe(steps => {
    //       this.getStepTypeHierarchy(steps).subscribe(stepsh => {
    //         this.buildStepHierarchy(result, stepsh);
    //       });
    //   });
    //
    //   return value;
    // }

    getProcessesSteps(process: Process, fromType: boolean): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.getListStep(process, fromType).subscribe(steps => {
                process.steps = steps;
                this.getStepTypeHierarchy(steps).subscribe(stepsh => {
                    const result = this.buildStepHierarchy(stepsh);
                    process.stepsTree = result;
                    resolve(true);
                });
            });
        });
    }

    // getProcessesSteps(process: Process, fromType: boolean): Observable<Step[]> {
    //   const result: Step[] = [];
    //   const value = of(result);
    //
    //   this.getListStep(process, fromType).subscribe(steps => {
    //     process.steps = steps;
    //     this.getStepTypeHierarchy(steps).subscribe(stepsh => {
    //       this.buildStepHierarchy(result, stepsh);
    //     });
    //   });
    //
    //   return value;
    // }

    getListStep(process: Process, fromType: boolean): Observable<Step[]> {
        const uri = fromType ? process.type : process.uri;
        const body = `query=` + encodeURIComponent(
            this.setQueryLanguage(PROCESS_STEPS_TYPES_LABEL_QUERY.replace('$processURI', uri))
        );
        return this.http.post<Step[]>(this.session.repoUrl, body, this.options).pipe(
            catchError(handleError),
            map(steps => {
                return this.extractSteps(steps).sort((a, b) => a.name.localeCompare(b.name));
            })
        );
    }

    getProcessTypeHierarchy(listProcess: Process[]): Observable<Process[]> {
        const knownTypes: string[] = [];
        const knownUnion: string[] = [];
        listProcess.forEach(process => {
            if (knownTypes.indexOf(process.type) === -1 && process.type !== '') {
                knownTypes.push(process.type);
                knownUnion.push(TEMPLATE_OBJECT_HIERARCHIE.replace('$objectURI', process.type));
            }
        });
        const union = knownUnion.join(' union ');
        const OBJECT_HIERARCHIE = union.trim().length === 0 ? OBJECT_FULL_HIERARCHIE : OBJECT_FILTERED_HIERARCHIE;
        const bodyh = `query=` + encodeURIComponent(
            this.setQueryLanguage(OBJECT_HIERARCHIE.replace('$TEMPLATE_OBJECT_HIERARCHIE', union)
                .replace(/\$topURI/g, 'http://opendata.inrae.fr/PO2/core/Process'))
        );
        return this.http.post<Process[]>(this.session.repoUrl, bodyh, this.options).pipe(
            catchError(handleError));
    }

    buildProcessHierarchy(processes: any): Process[] {
        const result: Process[] = [];
        const flatProcess = new Map<string, Process>();
        processes.results.bindings.map(process => {
            if (!flatProcess.has(process.father.value)) {
                const newProcess = new Process(null, null, process.fatherLabel.value, process.father.value);
                flatProcess.set(process.father.value, newProcess);
                result.push(newProcess);
            }
            if (!flatProcess.has(process.son.value)) {
                const newProcess = new Process(null, null, process.sonLabel.value, process.son.value);
                flatProcess.set(process.son.value, newProcess );
                result.push(newProcess);
            }
            flatProcess.get(process.father.value).children.push(flatProcess.get(process.son.value));
            const index = result.indexOf(flatProcess.get(process.son.value));
            if (index >= 0) {
              result.splice(index, 1);
            }
        });
        return result;
    }

    getStepTypeHierarchy(steps: Step[]): Observable<any> {
        const knownTypes: string[] = [];
        const knownUnion: string[] = [];
        if (steps.length === 0) {
            return of(null);
        }
        steps.forEach(step => {
            if (knownTypes.indexOf(step.typeUri) === -1 && step.typeUri !== '') {
                knownTypes.push(step.typeUri);
                knownUnion.push(TEMPLATE_OBJECT_HIERARCHIE.replace('$objectURI', step.typeUri));
            }
        });
        const union = knownUnion.join(' union ');
        const OBJECT_HIERARCHIE = union.trim().length === 0 ? OBJECT_FULL_HIERARCHIE : OBJECT_FILTERED_HIERARCHIE;
        const bodyh = `query=` + encodeURIComponent(
            this.setQueryLanguage(OBJECT_HIERARCHIE.replace('$TEMPLATE_OBJECT_HIERARCHIE', union)
                .replace(/\$topURI/g, 'http://opendata.inrae.fr/PO2/core/Step'))
        );
        return this.http.post<any>(this.session.repoUrl, bodyh, this.options).pipe(
            catchError(handleError));
    }

    buildStepHierarchy(steps: any): Step[] {
        const result: Step[] = [];
        const flatStep = new Map<string, Step>();
        if (steps !== null) {
            steps.results.bindings.map(step => {
                if (!flatStep.has(step.father.value)) {
                    const newStep = new Step(null, null, step.father.value, step.fatherLabel.value);
                    flatStep.set(step.father.value, newStep);
                    result.push(newStep);
                }
                if (!flatStep.has(step.son.value)) {
                    const newStep = new Step(null, null, step.son.value, step.sonLabel.value);
                    flatStep.set(step.son.value, newStep);
                    result.push(newStep);
                }
                flatStep.get(step.father.value).children.push(flatStep.get(step.son.value));
                const index = result.indexOf(flatStep.get(step.son.value));
                if (index >= 0) {
                  result.splice(index, 1);
                }
            });
        }
        return result;
    }

    getProcessStepTypes(process: Process): Step[] {
        const knownTypes: string[] = [];
        const types: Step[] = [];

        process.steps.forEach(step => {
            if (knownTypes.indexOf(step.type) === -1 && step.type !== '') {
                knownTypes.push(step.type);
                types.push(new Step('', '', step.typeUri, step.type));
            }
        });

        return types.sort((a, b) => a.name.localeCompare(b.name));
    }

    getStepMaterials(process: Process, step: Step): Observable<Filter[]> {
        this.session.datasetsLoading.push('loading');
        const processUri: string = (process && process.uri) ? process.uri : 'http://opendata.inrae.fr/PO2/core/Process';
        const stepURI: string = (step && step.uri) ? step.uri : step.typeUri;
        const queryAdress: string = this.session.repoUrl;
        const body = `query=` + encodeURIComponent(
            this.setQueryLanguage(STEP_MAT_QUERY
                .replace(/\$processURI/g, processUri)
                .replace(/\$stepURI/g, stepURI))
        );
        return this.http.post<any>(queryAdress, body, this.options).pipe(map(resp => {
            const res = this.extractMaterials(resp);
            this.session.datasetsLoading.splice(0, 1);
            return res;
        }));
    }

    private extractMaterials(resp: any): Filter[] {
        const res: Filter[] = [];

        resp.results.bindings.forEach(mat => {
            res.push(new Filter(mat.material_label.value, mat.material_type.value));
        });

        return res.sort(((a, b) => a.name.localeCompare(b.name)));
    }

    getStepInputComponent(process: Process, step: Step): Observable<Filter[]> {
        this.session.datasetsLoading.push('loading');
        const processUri: string = (process && process.uri) ? process.uri : 'http://opendata.inrae.fr/PO2/core/Process';
        const stepURI: string = (step && step.uri) ? step.uri : step.typeUri;

        const queryAdress: string = this.session.repoUrl;
        const body = `query=` + encodeURIComponent(
            this.setQueryLanguage(STEP_INPUT_QUERY
                .replace('$processURI', processUri)
                .replace(/\$stepURI/g, stepURI))
        );

        return this.http.post<any>(queryAdress, body, this.options).pipe(map(resp => {
            const res = this.extractInputComponent(resp);
            this.session.datasetsLoading.splice(0, 1);
            return res;
        }));
    }

    private extractInputComponent(resp: any): Filter[] {
        const res: Filter[] = [];

        resp.results.bindings.forEach(input => {
            res.push(new Filter(input.input_label.value, input.input_type.value));
        });

        return res.sort(((a, b) => a.name.localeCompare(b.name)));
    }

    getStepOutputComponent(process: Process, step: Step): Observable<Filter[]> {
        this.session.datasetsLoading.push('loading');
        const processUri: string = (process && process.uri) ? process.uri : 'http://opendata.inrae.fr/PO2/core/Process';
        const stepURI: string = (step && step.uri) ? step.uri : step.typeUri;
        const queryAdress: string = this.session.repoUrl;
        const body = `query=` + encodeURIComponent(
            this.setQueryLanguage(STEP_OUPUT_QUERY
                .replace('$processURI', processUri)
                .replace(/\$stepURI/g, stepURI))
        );

        return this.http.post<any>(queryAdress, body, this.options).pipe(map(resp => {
            const res = this.extractOutputComponent(resp);
            this.session.datasetsLoading.splice(0, 1);
            return res;
        }));
    }

    private extractOutputComponent(resp: any): Filter[] {
        const res: Filter[] = [];

        resp.results.bindings.forEach(output => {
            res.push(new Filter(output.output_label.value, output.output_type.value));
        });

        return res.sort(((a, b) => a.name.localeCompare(b.name)));
    }


    getListObservations(process: Process, uri: string): Observable<Observation[]> {
        const processUri: string = (process && process.uri) ? process.uri : 'http://opendata.inrae.fr/PO2/core/Process';
        const sampleURI = '[]';

        const queryAdress: string = this.session.repoUrl;
        const body = `query=` + encodeURIComponent(
            this.setQueryLanguage(STEP_OBS_TYPE_QUERY.replace('$processURI', processUri).replace(/\$stepURI/g, uri))
        );
        return this.http.post<any>(queryAdress, body, this.options).pipe(map(resp => {
            return this.extractObservations(resp);
        }));
    }

    getStepObservations(process: Process, step: Step): Observable<Observation[]> {
        this.session.datasetsLoading.push('loading');
        const result: Observation[] = [];
        const value = of(result);
        const stepURI: string = (step && step.uri) ? step.uri : step.typeUri;

        this.getListObservations(process, stepURI).subscribe(observations => {
            result.push(...observations);
            this.session.datasetsLoading.splice(0, 1);
            //    this.getObservationTypeHierarchy(observations).subscribe(observationsh => {
            //   this.buildObservationHierarchy(result, observationsh);
            //    });
        });
        return value;
    }

  getStepObservationMaterial(process: Process, selectedStep: Step, selectedObs: Observation): Observable<ObservationMaterial[]> {
      this.session.datasetsLoading.push('Loading');

      const processURI: string = (process && process.uri) ? process.uri : this.session.doNotFilterProcess.uri;
      const obsURI: string = (selectedObs && selectedObs.uri && selectedObs.uri === this.session.doNotFilterObservation.uri) ?
          '?obsi' : '<' + selectedObs.uri + '>';
      const stepURI: string = (selectedStep && selectedStep.name) ? selectedStep.uri : selectedStep.typeUri;

      const body = `query=` + encodeURIComponent(
          this.setQueryLanguage(
              OBS_MAT_QUERY.replace('$processURI', processURI).replace(/\$stepURI/g, stepURI).replace(/\$obsURI/g, obsURI)
          )
      );
      return this.http.post<any>(this.session.repoUrl, body, this.options).pipe(map(resp => {
          const availableMat = this.extractObsMaterials(resp);
          this.session.datasetsLoading.splice(0, 1);
          return availableMat;
      }));
  }

    getStepObservationPropertyUri(processURI: string, stepURI: string, selectedObs: Observation): Observable<Property[]> {
        this.session.datasetsLoading.push('Loading');
        let obsURI: string = '<' + selectedObs.uri + '>';
        if (selectedObs.uri === this.session.doNotFilterObservation.uri) {
            obsURI = '?obsi';
        }
        const queryAdress: string = this.session.repoUrl;
        const body = `query=` + encodeURIComponent(
            this.setQueryLanguage(STEP_OBS_PROP_QUERY.replace('$processURI', processURI).
            replace(/\$stepURI/g, stepURI).replace(/\$obsURI/g, obsURI))
        );
        return this.http.post<any>(queryAdress, body, this.options).pipe(map(resp => {
            const listProp = this.extractProperties(resp);
            this.session.datasetsLoading.splice(0, 1);
            return listProp;
        }));
    }

    getStepObservationProperty(process: Process, selectedStep: Step, selectedObs: Observation): Observable<Property[]> {
        const processUri: string = (process && process.uri) ? process.uri : this.session.doNotFilterProcess.uri;

        let stepURI: string;
        if (selectedStep.name) {
            stepURI = selectedStep.uri;
        } else {
            stepURI = selectedStep.typeUri;
        }

        return this.getStepObservationPropertyUri(processUri, stepURI, selectedObs);
    }


    // getStepProperties(process: Process, uri: string): Observable<Property[]> {
    //     const processUri: string = (process && process.uri) ? process.uri : 'http://opendata.inrae.fr/PO2/core/Transformation_Process';
    //     const sampleURI = '[]';
    //     const comment: string = (process && !process.uri) ? '' : '#';
    //     const queryAdress: string = this.session.repoUrl;
    //     const body = `query=` + encodeURIComponent(
    //         this.setQueryLanguage(STEP_PROP_QUERY.replace('$processURI', processUri).
    //         replace(/\$sampleURI/g, sampleURI).replace(/\$comment/g, comment).replace(/\$stepURI/g, uri))
    //     );
    //     return this.http.post<any>(queryAdress, body, this.options).pipe(map(resp => {
    //       const listProp = this.extractProperties(resp);
    //       return listProp;
    //     }));
    // }

    async getConvertedUnit(unit: string, value: string) {

        const body = `unit=` + encodeURIComponent(unit) + `&value=` + encodeURIComponent(value);
        return await this.http.post<any>(local + getSIQuantity, body, this.options).toPromise();
    }

    publishToDataverse(dataset: Dataverset, update: boolean): Observable<boolean> {

        const form = new FormData();
        form.append('title', dataset.title);
        form.append('description', dataset.description);
        form.append('authors', dataset.authors);
        form.append('keywords', dataset.keywords);
        form.append('subjects', dataset.subjects);
        form.append('landingpage', dataset.landingpage);
        form.append('save', JSON.stringify(dataset.save));
        form.append('filePrefix', dataset.filePrefix);

        for (const f of dataset.files) {
            form.append('file', f, f.name);
        }

        let addr = '';
        if (!update) {
            addr = publishDataverse;
        } else {
            form.append('datasetID', dataset.datasetID);
            form.append('datasetDOI', dataset.doi);
            addr = updateDataverse;
        }
        return this.http.post<any>(local + addr, form).pipe(
            catchError(error => {
                console.log('error : ' + error);
                return [error.status];
            }),
            map(res => {
                console.log('res : ' + res);
                return res.datasetDoi;
            }));
    }

    getPrettyPrintUnit(unit: string): Observable<string> {

        const body = `unit=` + encodeURIComponent(unit);
        return this.http.post<any>(local + checkUnit, body, this.options).pipe(
            catchError(error => {
                return [error.error];
            }),
            map(res => {
                return res.value;
            }));
    }

    private extractProperties(resp: any): Property[] {
        const listProp: Property[] = [];

        resp.results.bindings.forEach(output => {

            // Extract unit info
            const unit = (output.unit_label) ? output.unit_label.value : null;
            if (unit) {
                this.getPrettyPrintUnit(unit).subscribe(value => {
                    this.prettyUnit.set(unit, value);
                });
            }
            // check if property existe
            let ind = listProp.map(value => value.uri).indexOf(output.prop_type.value);
            if (ind < 0) {
                // -1 because the return value is not the index but the length of the array
                ind = listProp.push(new Property(output.prop_type_label.value, output.prop_type.value)) - 1;
            }
            if (unit) {
                if (listProp[ind].availableUnits.indexOf(unit) < 0) { // add unit if not already in list
                    listProp[ind].availableUnits.push(unit);
                }
            }

            // ,Extract Object Of Interest info
            const objType = (output.obj_type) ? output.obj_type.value : null;
            const objLabel = (output.obj_label) ? output.obj_label.value : 'no name found';
            let objObj = null;
            if (objType) {
            objObj = new ObjectOI(objLabel, objType);
            }

            // add prop if not exist
            let indOOF = listProp.map(value => value.uri).indexOf(output.prop_type.value);
            if (indOOF < 0) {
              // tslint:disable-next-line:max-line-length
              indOOF = listProp.push(new Property(output.prop_type_label.value, output.prop_type.value)) - 1; // -1 because the return value is not the index but the length of the array
            }
            if (objObj) {
              if (listProp[indOOF].availableObjects.map(value => value.uri).indexOf(objType) < 0) { // add obj if not already in list
                listProp[indOOF].availableObjects.push(objObj);
              }
          }
        });

        return listProp.sort(((a, b) => a.name.localeCompare(b.name)));
    }

    private extractObservations(resp: any): Observation[] {
        const res: Observation[] = [];

        resp.results.bindings.forEach(output => {
            res.push(new Observation(output.obs_ind_label.value, output.obs_ind.value));
        });

        return res.sort(((a, b) => a.name.localeCompare(b.name)));
    }

    getStepMaterialParams(process: Process, step: Step, stepMaterial: Filter): Observable < Measurement[] > {
        const processUri: string = (process && process.uri) ? process.uri : this.session.doNotFilterProcess.uri;
        const stepURI: string = (step && step.uri) ? step.uri : step.typeUri;
        const queryAdress: string = this.session.repoUrl;
        const body = `query=` + encodeURIComponent(
            this.setQueryLanguage(
                STEP_MATERIALS_PARAMS_QUERY.replace('$processURI', processUri).replace('$materialURI', stepMaterial.uri).replace(/\$stepURI/g, stepURI)
            )
        );
        return this.http.post<any>(queryAdress, body, this.options).pipe(map(resp => {
            stepMaterial.measurementsAvailable = this.extractStepMaterialParams(resp).sort(((a, b) => a.name.localeCompare(b.name)));
            return stepMaterial.measurementsAvailable;
        }));
    }

    private extractStepMaterialParams(resp: any): Measurement[] {
        const res: Measurement[] = [];

        resp.results.bindings.forEach(output => {
            const unit = (output.unit_label) ? output.unit_label.value : null;
            if (unit) {
                this.getPrettyPrintUnit(unit).subscribe(value => {
                    this.prettyUnit.set(unit, value);
                });
            }
            // check if property existe
            const ind = res.map(value => value.uri).indexOf(output.car_URI.value);
            if (ind >= 0) {
                if (unit) {
                    res[ind].availableUnits.push(unit);
                }
            } else {
                if (unit) {
                    res.push(new Measurement(output.car_label.value, 'Value', null, [unit], output.car_URI.value));
                } else {
                    res.push(new Measurement(output.car_label.value, 'Value', null, [], output.car_URI.value));
                }
            }
        });

        return res;
    }

    getComponentIngredients(process: Process, step: Step, comp: Filter, isInput: boolean): Observable < Measurement[] > {
        const processUri: string = (process && process.uri) ? process.uri : this.session.doNotFilterProcess.uri;
        const stepURI: string = (step && step.uri) ? step.uri : step.typeUri;
        const queryAdress: string = this.session.repoUrl;
        const queryTemplate = (isInput) ? STEP_INPUT_COMPONENT_INGREDIENTS_QUERY : STEP_OUTPUT_COMPONENT_INGREDIENTS_QUERY;
        const uriToReplace = (isInput) ? '\\$inputURI' : '\\$outputURI';
        const regToReplace = new RegExp(uriToReplace, 'g');
        const body = `query=` + encodeURIComponent(
            this.setQueryLanguage(
                queryTemplate.replace('$processURI', processUri).replace(regToReplace, comp.uri).replace(/\$stepURI/g, stepURI)
            )
        );
        return this.http.post<any>(queryAdress, body, this.options).pipe(map(resp => {
            comp.measurementsAvailable = this.extractComponentIngredient(resp);
            return this.trimEmptyMeasurements(comp.measurementsAvailable);
        }));
    }

    private extractComponentIngredient(resp: any): Measurement[] {
        const res: Measurement[] = [];

        resp.results.bindings.forEach(output => {
            const unit = (output.unit_label) ? output.unit_label.value : null;
            if (unit) {
                this.getPrettyPrintUnit(unit).subscribe(value => {
                    this.prettyUnit.set(unit, value);
                });
            }
            // check if property existe
            const ind = res.map(value => value.uri).indexOf(output.ingr_type.value);
            if (ind >= 0) {
                if (unit) {
                    res[ind].availableUnits.push(unit);
                }
            } else {
                if (unit) {
                    res.push(new Measurement(output.ingr_type_label.value, output.car_label.value, null, [unit], output.ingr_type.value));
                } else {
                    res.push(new Measurement(output.ingr_type_label.value, output.car_label.value, null, [], output.ingr_type.value));
                }
            }
        });

        return res;
    }

    // getPropertyData(processUri: string, stepUri: string, obsUri: string, property: Property): Observable < Property > {
    //     this.session.datasetsLoading.push('Loading');
    //     processUri = (processUri) ? processUri : this.session.doNotFilterProcess.uri;
    //     const queryAdress: string = this.session.repoUrl;
    //     if (obsUri === this.session.doNotFilterObservation.uri) {
    //         obsUri = '?obs_i';
    //     } else {
    //         obsUri = '<' + obsUri + '>';
    //     }
    //     const body = `query=` + encodeURIComponent(
    //         this.setQueryLanguage(
    //             OBS_MAT_QUERY.replace('$processURI', processUri).replace(/\$stepURI/g, stepUri).replace(/\$obsURI/g, obsUri)
    //         )
    //     );
    //     return this.http.post<any>(queryAdress, body, this.options).pipe(map(resp => {
    //         // property.observationMaterials = this.extractObsMaterials(resp);
    //         this.session.datasetsLoading.splice(0, 1);
    //         return property;
    //     }));
    // }

    private extractObsMaterials(resp: any): ObservationMaterial[] {
        const res: ObservationMaterial[] = [];

        resp.results.bindings.forEach(mat => {
            res.push(new ObservationMaterial(mat.sensor_label.value, mat.sensor_type.value));
        });

        return res.sort(((a, b) => a.name.localeCompare(b.name)));
    }

    extractDatasetsName(datasets: any): Dataset[] {
        return datasets.map(dataset => {
            return new Dataset(dataset.id, dataset.group, dataset.location);
        });
    }

    extractProcesses(processes: any): Process[] {
        return processes.results.bindings.map(proc => {
            return new Process(
                proc.process_instance_label.value,
                proc.process_instance.value,
                proc.process_type_label.value,
                proc.process_type.value);
        });
    }


    extractSteps(steps: any): Step[] {
        return steps.results.bindings.map(step => {
            return new Step(step.stepi.value, step.stepi_label.value, step.step_type.value, step.step_type_label.value);
        });
    }

    trimEmptyAndDuplicateTypes(steps: Step[]): Step[] {
        const res: Step[] = [];
        steps.forEach(step => {
            if (step.type !== '' && res.findIndex(e => e.typeUri === step.typeUri) === -1) {
                res.push(step);
            }
        });

        return res;
    }

    trimEmptyMeasurements(measurements: Measurement[]): Measurement[] {
        const res: Measurement[] = [];
        measurements.forEach(measurement => {
            if (measurement.name !== '') {
                res.push(measurement);
            }
        });

        return res;
    }

    addSparqlQuery(url: string, query: string): string {
        if (url.indexOf('?') === -1) {
            return url + '?query=' + encodeURIComponent(this.setQueryLanguage(query));
        } else {
            return url + '&query=' + encodeURIComponent(this.setQueryLanguage(query));
        }
    }

    setQueryLanguage(query: string): string {
        return query.replace(/\$lang/g, this.session.language);
    }

    // Not implemented yet, do not use
    getObsMaterialParams(processUri: string, stepUri: string, obsUri: string, obsMaterial: ObservationMaterial):
        Observable<ObsControlParam[]> {
        processUri = (processUri) ? processUri : 'http://opendata.inrae.fr/PO2/core/Transformation_Process';
        let queryAdress: string = this.session.repoUrl;

        queryAdress = this.addSparqlQuery(
            queryAdress,
            OBS_MATERIAL_PARAMS_QUERY.replace('$processURI', processUri).replace('$samplerURI', obsMaterial.uri)
              .replace(/\$stepURI/g, stepUri).replace(/\$obsURI/g, obsUri)
        );
        return this.http.get<any>(queryAdress).pipe(map(resp => {
            obsMaterial.obsContParam = this.extractObsMaterialParams(resp);
            return obsMaterial.obsContParam;
        }));
    }

    private extractObsMaterialParams(resp: any): ObsControlParam[] {  // Not implemented yet, do not use
        const res: ObsControlParam[] = [];

        console.log(resp);
        resp.results.bindings.forEach(param => {
            res.push(new ObsControlParam('', ''));
        });
        console.log(res);

        return res.sort(((a, b) => a.name.localeCompare(b.name)));
    }


}

