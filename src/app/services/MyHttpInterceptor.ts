import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginService} from './login.service';

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
  constructor(public auth: LoginService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // Clone the request to add the new header if a user login exist
    if (this.auth.getcurrentUser() === null) {
      console.log('no user connected. sending original request');
      return next.handle(request);
    } else {
      request = request.clone({ headers: request.headers.set('Authorization', 'Basic ' + this.auth.getcurrentUser().token) });
      console.log('add auth in clone request');
      return next.handle(request);
    }
  }
}
