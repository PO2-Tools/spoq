import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {local, getSaveList, login, getDatasetList} from 'src/assets/request-adresses';
import { catchError, map } from 'rxjs/operators';
import {User} from '../model/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }


  checkIfLoggedIn(): Observable<boolean> {

    return this.http.post<any>(local + login, {}).pipe(
      catchError(error => {
        return [error];
      }),
      map(a => {
        if (!a || a.status === 401) {
          return false;
        } else {
          return true;
        }
      }));

    // const res = this.getUserSaveList().pipe(
    //   catchError(error => {
    //     return [error.error];
    //   }),
    //   map(a => {
    //     if (!a || a.error === 'Unauthorized') {
    //       return false;
    //     } else {
    //       return true;
    //     }
    //   }));
    //
    // return res;
  }

  getUserSaveList(): Observable<string[][]> {
    return this.http.post<string[][]>(local + getSaveList, {});
  }

  getDatasetList(): Observable<string[]> {
    return this.http.post<string[]>(local + getDatasetList, {});
  }
}
