import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of, throwError} from 'rxjs';
import { SessionService } from './session.service';
import { delay, catchError, map, switchMap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {local, login, logout} from 'src/assets/request-adresses';
import {User} from '../model/User';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private currentUser: User;

  constructor( private http: HttpClient) {
    this.currentUser = null;
  }

  public getcurrentUser(): User {
    return this.currentUser;
  }

  login(username, password) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + btoa(username + ':' + password)
      }),
    };

    return this.http.post<any>(local + login, {}, httpOptions).pipe(
      catchError(error => {
        return [error.error];
      }),
      map(a => {
            if (a && (a.error || a.type === 'error')) {
              this.currentUser = null;
              return false;
            } else {
              this.currentUser = new User();
              this.currentUser.username = username;
              this.currentUser.token = btoa(username + ':' + password);
              return true;
            }
      }));
  }

  logout(): Observable<boolean> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'text/plain;charset=UTF-8',
        'Accept': '*/*',
      }),
    };
    const res = this.http.post(local + logout, {}, httpOptions).pipe(
      catchError(error => {
        return [error.error];
      }),
      map(a => {
        if (a && (a.error || a.type === 'error')) {
          return false;
        } else {
          this.currentUser = null;
          return true;
        }
      }));

    return res;
  }
}
