import { TestBed } from '@angular/core/testing';

import { ResultsFormatConverterService } from './results-format-converter.service';

describe('ResultsFormatConverterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResultsFormatConverterService = TestBed.get(ResultsFormatConverterService);
    expect(service).toBeTruthy();
  });
});
