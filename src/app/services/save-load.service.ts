import { Injectable } from '@angular/core';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import { Dataset } from '../model/Dataset';
import { EditorSave } from '../model/EditorSave';
import { FormParameters } from '../model/FormParameters';
import { FormSave } from '../model/FormSave';
import { WholeSave } from '../model/WholeSave';
import { InitDataService } from './init-data.service';
import { SessionService } from './session.service';
import { UserService } from './user.service';
import {local, saveEditorState, saveFormState, saveWholeState} from '../../assets/request-adresses';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Filter} from '../model/Filter';
import {Process} from '../model/Process';

@Injectable({
  providedIn: 'root'
})
export class SaveLoadService {
  readonly options = {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  };

  constructor(
    private sessionsService: SessionService,
    private initDataService: InitDataService,
    private userService: UserService,
    private http: HttpClient
  ) { }

  saveFormState(formState: FormParameters, saveName: string, saveDescription: string): Observable<boolean> {
    console.log('Sending form state to server for saving purpose as:', saveName);
    console.log(formState);
    let success: Observable<boolean>;

    const body = `save=${encodeURIComponent(JSON.stringify(formState))}
      &saveName=${encodeURIComponent(saveName)}
      &saveDesc=${encodeURIComponent(saveDescription)}`;

    success = this.http.post<boolean>(local + saveFormState, body, this.options).pipe(
      catchError(err => {
        return of(true); // true pour indiquer qu'il y a une erreur.
      }),
      map(a => {
        return !a;
      }));
    return success;

    // success = of(FORM_SAVES.findIndex(e => {
    //   return e.name === saveName;
    // }) === -1).pipe(switchMap(resp => {
    //   if (resp) {
    //     return of(FORM_SAVES.push({ save: formState, name: saveName }) !== -1);
    //   } else {
    //     return of(false);
    //   }
    // }));
    //
    // return success; // return true if save was completed -> name was available
  }

  saveEditorState(text: string, datasets: Dataset[], saveName: string, saveDescription: string): Observable<boolean> {
    console.log('Sending editor state to server for saving purpose as:', saveName);
    let success: Observable<boolean>;

    const save = new EditorSave();
    save.name = saveName;
    save.selectedDatasetsForEditor = datasets;
    save.editorSave = text;

    const body = `save=${encodeURIComponent(JSON.stringify(save))}
      &saveName=${encodeURIComponent(saveName)}
      &saveDesc=${encodeURIComponent(saveDescription)}`;

    success = this.http.post<boolean>(local + saveEditorState, body, this.options).pipe(
      catchError(err => {
        return of(true); // true pour indiquer qu'il y a une erreur.
      }),
      map(a => {
        return !a;
      }));
    return success;
  }

  // tslint:disable-next-line:max-line-length
  saveBoth(text: string, formState: FormParameters, saveName: string, saveDescription: string, selectedDatasetsForEditor: Dataset[]): Observable<boolean> {
    console.log('Sending form and editor state to server for saving purpose as:', saveName);

    const editorSave = new EditorSave();
    editorSave.name = saveName;
    editorSave.selectedDatasetsForEditor = selectedDatasetsForEditor;
    editorSave.editorSave = text;

    const formSave = new FormSave();
    formSave.name = saveName;
    formSave.save = formState;

    const wholeSave = new WholeSave();
    wholeSave.editorSave = editorSave;
    wholeSave.formSave = formSave;
    wholeSave.name = saveName;

    let success: Observable<boolean>;

    const body = `save=${encodeURIComponent(JSON.stringify(wholeSave))}
      &saveName=${encodeURIComponent(saveName)}
      &saveDesc=${encodeURIComponent(saveDescription)}`;

    success = this.http.post<boolean>(local + saveWholeState, body, this.options).pipe(
      catchError(() => {
        return of(true); // true pour indiquer qu'il y a une erreur.
      }),
      map(a => {
        return !a;
      }));
    return success;
  }

  loadFormState(formSave: FormSave): Observable<boolean> {
    // const formSave = new FormSave();
    // formSave.name = save.saveName;
    // formSave.save = JSON.parse(save.save);

    // console.log('Loading form save:', save.saveName);
    //
    // const save = JSON.parse(saveJSON) as FormSave; // stubbed part
    // return this.initDataService.getRepositoryAdress(save.save.selectedDatasets).pipe(map(resp => {
    //       const success = resp.location !== '';
    //       if (success) {
    //         this.sessionsService.repoUrl = resp.location;
    //         this.assignFormdata(save.save);
    //         this.sessionsService.justLoaded.push(success);
    //       }
    //       return success;
    //     }));
    if (formSave && formSave.save.selectedDatasets.length > 0) {
      return this.initDataService.updateRepositoryAdress(formSave.save.selectedDatasets, true).pipe(map(isUpdated => {
        console.log('repository location is update : ' + isUpdated);
        this.assignFormdata(formSave.save);
        this.sessionsService.justLoaded.push(isUpdated);
        return isUpdated;
      }));
    }
  }

  assignFormdata(res: FormParameters): void {
    this.sessionsService.selectedParameters = res;
    this.sessionsService.availableDatasets = [];

    this.initDataService.getInitData().subscribe(data => {
      this.sessionsService.availableDatasets = data;
      // update selectedDatasets with the new availableDatasets to prevent ngModel failed !
      const newDatasets: Dataset[] = [];
      this.sessionsService.selectedParameters.selectedDatasets.forEach(oldDataset => {
        newDatasets.push(this.sessionsService.availableDatasets.filter(d => d.location === oldDataset.location)?.at(0));
      });
      this.sessionsService.selectedParameters.selectedDatasets = newDatasets;
      this.initDataService.getListProcesses(this.sessionsService.selectedParameters.selectedDataset).subscribe(result => {
        console.log('all process ' + result);
        // update selectedProcess with the new available process to prevent ngModel failed !
        let newProcess: Process = result.filter(oldProcess => oldProcess
            .equals(this.sessionsService.selectedParameters.selectedProcess))?.at(0);
        this.sessionsService.datasetsLoading.splice(0, 1);
        this.sessionsService.datasetsLoading.push('loading');
        this.initDataService.getDatasetProcessTree(result).then(processTree => {
          console.log('tree done');
          this.sessionsService.availableProcessTree = processTree;
          if (!newProcess) {
            newProcess = this.sessionsService.availableProcessTree?.at(0).flatten()
                .filter(oldProcess => oldProcess.equals(this.sessionsService.selectedParameters.selectedProcess))?.at(0);
          }
          this.sessionsService.selectedParameters.selectedProcess = newProcess;
          this.sessionsService.datasetsLoading.splice(0, 1);
          // force to update available value for all objects !
          this.sessionsService.selectedParameters.selectedSteps.forEach(step => {
            this.initDataService.getStepMaterials(this.sessionsService.selectedParameters.selectedProcess, step)
                .subscribe(filter => step.filterAvailable[Filter.MATERIAL] = filter);
            this.initDataService.getStepInputComponent(this.sessionsService.selectedParameters.selectedProcess, step)
                .subscribe(filter => step.filterAvailable[Filter.INPUTCOMPONENT] = filter);
            this.initDataService.getStepOutputComponent(this.sessionsService.selectedParameters.selectedProcess, step)
                .subscribe(filter => step.filterAvailable[Filter.OUTPUTCOMPONENT] = filter);
            this.initDataService.getStepObservations(this.sessionsService.selectedParameters.selectedProcess, step)
                .subscribe(filter => step.observationsAvailable = filter);
            step.observations.forEach(obs => {
              this.initDataService.getStepObservationProperty(this.sessionsService.selectedParameters.selectedProcess, step, obs)
                  .subscribe(resultObs => obs.availableProperties = resultObs);
              this.initDataService.getStepObservationMaterial(this.sessionsService.selectedParameters.selectedProcess, step, obs)
                  .subscribe(resultObs => obs.availableMaterial = resultObs);
            });
          });

        });
      });
    });
  }

  loadEditorState(editorSave: EditorSave): Observable<boolean> {
    // const saveJSON = JSON.parse(save);
    // const editorSave = save;
    // editorSave.name = saveJSON.name;
    // editorSave.selectedDatasetsForEditor = saveJSON.selectedDatasetsForEditor;
    // editorSave.editorSave = saveJSON.editorSave;
    this.clearEditor();
    return of(editorSave).pipe(switchMap(res => {
      console.log(res.selectedDatasetsForEditor);
      if (res && res.selectedDatasetsForEditor.length > 0) {
        return this.initDataService.getRepositoryAdress(res.selectedDatasetsForEditor)
          .pipe(map(resp => {
            const success = resp.location !== '';
            if (success) {
              this.sessionsService.repoUrlSparqlEditor = resp.location;
              this.affectEditordata(res);
            }
            return success;
          }));
      } else {
        return of(true);
      }
    }));
  }

  loadWholeState(savea: any): Observable<boolean> {

    try {
      const saveJSON = JSON.parse(savea.save);

      const editorSave = new EditorSave();
      editorSave.name = saveJSON.editorSave.name;
      editorSave.selectedDatasetsForEditor = saveJSON.editorSave.selectedDatasetsForEditor;
      editorSave.editorSave = saveJSON.editorSave.editorSave;

      const formSave = new FormSave();
      formSave.name = saveJSON.formSave.name;
      formSave.save = saveJSON.formSave.save;

      const wholeSave = new WholeSave();
      wholeSave.editorSave = editorSave;
      wholeSave.formSave = formSave;
      wholeSave.name = saveJSON.name;


      return of(wholeSave).pipe(switchMap(res => {
        this.loadFormState(res.formSave).subscribe(value => {
          console.log('form load : ' + value);
        });
        this.loadEditorState(res.editorSave).subscribe(value1 => {
          console.log('editor load : ' + value1);
        });
        return of(true);
        // const formSuccess = this.initDataService.getRepositoryAdress(res.formSave.save.selectedDatasets)
        //   .pipe(map(resp => {
        //     const success = resp.location !== '';
        //     if (success) {
        //       this.sessionsService.repoUrl = resp.location;
        //       this.assignFormdata(res.formSave.save);
        //     }
        //     return success;
        //   }), switchMap(resp => {
        //     if (resp && res.editorSave.selectedDatasetsForEditor.length > 0 && !res.editorSave.editorSave) {
        //       return this.initDataService.getRepositoryAdress(res.editorSave.selectedDatasetsForEditor)
        //         .pipe(map(response => {
        //           const success = response.location !== '';
        //           if (success) {
        //             this.sessionsService.repoUrlSparqlEditor = response.location;
        //             this.affectEditordata(res.editorSave);
        //           }
        //           return success;
        //         }));
        //     } else {
        //       this.sessionsService.repoUrlSparqlEditor = '';
        //       this.clearEditor();
        //       return of(resp);
        //     }
          }));


        // return formSuccess;
      // }));
    } catch (e) {
      return of(false);
    }
  }

  affectEditordata(save: EditorSave): void {
    this.sessionsService.selectedEditorDatasets = save.selectedDatasetsForEditor;
    this.sessionsService.editorContent = save.editorSave;
  }

  clearEditor(): void {
    this.sessionsService.selectedEditorDatasets.length = 0;
    this.sessionsService.editorContent = '';
  }

  getSaveList(isAvancedUser: boolean): Observable<string[][]> {
    return this.userService.getUserSaveList();
  }

  getDatasetList(): Observable<string[]> {
    return this.userService.getDatasetList();
  }
}
