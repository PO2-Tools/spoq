import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { InitDataService} from './init-data.service';
import { FormParameters } from '../model/FormParameters';
import { SparqlTemplate } from '../model/SparqlTemplate';
import { Process } from '../model/Process';
import { Step } from '../model/Step';
import {
  PREFIX, FILTER_STEP, FILTER_STEP_MAT, FILTER_STEP_MAT_PARAM, FILTER_STEP_INPUT_OUTPUT, FILTER_STEP_INPUT_OUTPUT_INGR,
  FILTER_QUANTITY, FILTER_QUALITY, FILTER_QUALITY_FILTER, FILTER_QUANTITY_MAX, FILTER_QUANTITY_MIN, FILTER_QUANTITY_EXACT,
  FILTER_STEP_PROP, FILTER_STEP_PROP_MAT, FILTER_STEP_OBS, ADD_OBJ_OBS_PROP, FILTER_OBS_PROP_OBJECT
} from '../sparql-templates';
import { Filter } from '../model/Filter';
import { Measurement } from '../model/Measurement';
import { Property } from '../model/Property';
import { ObservationMaterial } from '../model/ObservationMaterial';
import {Observation} from '../model/Observation';
import {Observable, of} from 'rxjs';
import {ObjectOI} from '../model/ObjectOI';

@Injectable({
  providedIn: 'root'
})
export class SparqlEditorService {

  selectVars: string[];

  constructor(private sessionService: SessionService, private initDataService: InitDataService) { }

  async getFromForm(): Promise<string> {
    return await this.getSparqlQuery();
  }

  async generateTemplates(): Promise<SparqlTemplate[]> {
    const params: FormParameters = this.sessionService.selectedParameters;
    const res: SparqlTemplate[] = [];
    this.selectVars = [];
    console.log('using form params to generate templates');
    res.push(new SparqlTemplate('', 'prefixes'));

    let index = 0;
    for (const step of params.selectedSteps) {
      index++;
      res.push(await this.populateStepTemplate(step, params.selectedProcess, index));
    }

    res[0].sparql = PREFIX.replace('$selectList', this.printSelectVars());

    return res;
  }

  private async populateStepTemplate(step: Step, process: Process, index: number): Promise<SparqlTemplate> {
    let res: SparqlTemplate;

    //  FILTER_STEP
    res = this.addStepTitleTemplate(step, process, index);

    if (step.doNotFilter) {
      res.title = 'Unfiltered step ' + index;
    } else if (step.name) {
      res.title = 'Step ' + index + ' filtered by name: ' + step.name;
    } else if (step.type) {
      res.title = 'Step ' + index + ' filtered by type: ' + step.type;
    } else {
      return new SparqlTemplate('New Step', 'Please select a filter type');
    }

    // FILTER_STEP_MAT
    if (step.filter[Filter.MATERIAL]) {
      res.subTemplates.push(await this.addStepMaterialFilter(step, index));
    }

    // FILTER_STEP_INPUT_OUTPUT input
    if (step.filter[Filter.INPUTCOMPONENT]) {
      res.subTemplates.push(await this.addIOComponentFilter(step, index, true));
    }

    // FILTER_STEP_INPUT_OUTPUT output
    if (step.filter[Filter.OUTPUTCOMPONENT]) {
      res.subTemplates.push(await this.addIOComponentFilter(step, index, false));
    }

    // FILTER_STEP_PROP + (FILTER_QUALITY || FILTER_QUANTITY)
    let indexObs = 0;
    for (const obs of step.observations) {
      indexObs++;
      res.subTemplates.push(await this.addObservationFilter(step, obs, index, indexObs));
    }



    return res;
  }

  private addStepTitleTemplate(step: Step, process: Process, index: number): SparqlTemplate {
    let templateString = FILTER_STEP;
    const projectLabel = '?project_name_' + index;
    const processLabel = '?process_' + index;
    const itinerary = '?itinerary';
    const itineraryLabel = '?itineraryLabel';
    const directProcess = '?direct_process_' + index;
    const stepLabel = '?step_' + index;
    const unfilteredProcess = '<http://opendata.inrae.fr/PO2/core/Process>';

    templateString = templateString.replace(/\$projectVarName/g, projectLabel);
    templateString = templateString.replace(/\$processVarLabel/g, processLabel);
    templateString = templateString.replace(/\$itineraryVar/g, itinerary);
    templateString = templateString.replace(/\$itineraryVarLabel/g, itineraryLabel);
    templateString = templateString.replace(/\$directProcess/g, directProcess);
    templateString = templateString.replace(/\$stepIndLabel/g, stepLabel);
    templateString = templateString.replace(/\$stepType/g, (step.name || step.doNotFilter) ? '?stepTypeURI_' + index : `<${step.typeUri}>`);
    templateString = templateString.replace(/\$typeDirectType/g, (step.name || step.doNotFilter) ? 'sesame:directType' : 'rdf:type');
    templateString = templateString.replace(/\$processVar/g, (!process || !process.uri) ? unfilteredProcess : `<${process.uri}>`);
    templateString = this.setStepInd(step, templateString, index);
    templateString = this.setLanguage(templateString);

   // if (process && process.uri !== this.sessionService.getDoNotFilterProcess().uri) {
    this.selectVars.push(projectLabel);
    this.selectVars.push(processLabel);
    this.selectVars.push(itineraryLabel);
   // }
    this.selectVars.push(stepLabel);

    return new SparqlTemplate('', templateString);
  }

  private async addStepMaterialFilter(step: Step, index: number): Promise<SparqlTemplate> {
    let res: SparqlTemplate;
    let templateString = FILTER_STEP_MAT;
    const materialLabel = '?stepMaterial_' + index;

    templateString = this.setStepInd(step, templateString, index);  // même $stepInd que pour l'étape
    templateString = templateString.replace(/\$materialType/g, `<${step.filter[Filter.MATERIAL].uri}>`);  // $samplerType = URI du materiel
    templateString = templateString.replace(/\$materialIndLabel/g, materialLabel);
    templateString = this.setActuatorInd(templateString, index); // $actuatorInd = variable sparql
    templateString = this.setMaterialInd(templateString, index); // $materialInd = variable sparql
    templateString = this.setLanguage(templateString);

    this.selectVars.push(materialLabel);

    res = new SparqlTemplate('Step Material filter', templateString);

    // FILTER_STEP_MAT_PARAM
    res.subTemplates.push(await this.addMaterialParameterFilter(step.filter[Filter.MATERIAL], index));

    return res;
  }

  private async addIOComponentFilter(step: Step, index: number, io: boolean): Promise<SparqlTemplate> {
    let res: SparqlTemplate;
    let templateString = FILTER_STEP_INPUT_OUTPUT;
    const prefix = (io) ? 'Input' : 'Output';

    const component: Filter = step.filter[(io) ? Filter.INPUTCOMPONENT : Filter.OUTPUTCOMPONENT];
    const inputOutput = '?' + prefix.toLocaleLowerCase() + `_${index}`;
    const inputOutputLabel = '?' + prefix.toLocaleLowerCase() + `Component_${index}`;

    // $property -> si input alors PO2:hasInput -> si output alors PO2:hasOutput
    templateString = templateString.replace(/\$inputOutputLabel/g, inputOutputLabel);
    // $inputOutput = variable sparql
    templateString = this.setinputOutput(templateString, inputOutput);
    // $property -> si input alors PO2:hasInput -> si output alors PO2:hasOutput
    templateString = templateString.replace(/\$property/g, 'PO2:has' + prefix);
    // $component = URI du component
    templateString = templateString.replace(/\$component/g, `<${component.uri}>`);
    // même $steInd que pour l'étape
    templateString = this.setStepInd(step, templateString, index);
    templateString = this.setLanguage(templateString);

    this.selectVars.push(inputOutputLabel);

    res = new SparqlTemplate(prefix + ' Component filter', templateString);

    // FILTER_STEP_INPUT_OUTPUT_INGR
    res.subTemplates.push(await this.addComponentIngredient(component, index, inputOutput, prefix));

    return res;
  }

  private async addObservationFilter(step: Step, obs: Observation, index: number, indexObs: number): Promise<SparqlTemplate> {
    // tslint:disable-next-line:prefer-const
    let res: SparqlTemplate;
    let templateString = FILTER_STEP_OBS;

    if (!obs.uri) {
      return new SparqlTemplate('New Observation filter', 'Please select an observation');
    }

    let obsURI: string = '<' + obs.uri + '>';
    if (obs.uri === this.sessionService.doNotFilterObservation.uri) {
      obsURI = '?obsInd_' + index + indexObs;
    }
    const obsLabel = '?observation_' + index + indexObs;

    templateString = this.setStepInd(step, templateString, index);  // même stepInd que pour FILTER_STEP
    templateString = templateString.replace(/\$obsInd/g, obsURI);  // $obsInd = variable sparql
    templateString = templateString.replace(/\$obsLabel/g, obsLabel); // $obsLabel = variable sparql
    this.selectVars.push(obsLabel);
    templateString = this.setLanguage(templateString);

    res = new SparqlTemplate('Observation filter ' + indexObs, templateString);

    // FILTER_OBSERVATION_MATERIAL
    let indexMat = 0;

    for (const mat of obs.includeMaterial) {
      if (mat) {
        indexMat++;
        res.subTemplates.push(this.addObservationMaterial(step, mat, index, indexObs, indexMat));
      }
    }


    // FILTER_STEP_PROP + (FILTER_QUALITY || FILTER_QUANTITY)
    let indexProp = 0;
    for (const prop of obs.properties) {
      indexProp++;
      res.subTemplates.push(await this.addPropertyFilter(step, obs, prop, index, indexObs, indexProp));
    }

    return res;
  }

  // tslint:disable-next-line:max-line-length
  private async addPropertyFilter(step: Step, obs: Observation, prop: Property, index: number, indexObs: number, indexProp: number): Promise<SparqlTemplate> {
    let res: SparqlTemplate;
    let templateString = FILTER_STEP_PROP;
    let templateObject = ADD_OBJ_OBS_PROP;
    const resultVar = '?propertyResult_' + index + indexObs + indexProp;
    const objectVar = '?objectInd_' + index + indexObs + indexProp;
    const objectLabel = '?objectLabel_' + index + indexObs + indexProp;
    const propLabel = '?property_' + index + indexObs + indexProp;
    const collectionVar = '?collectionInd_' + index + indexObs;
    const collectionVarTop = '?collectionTop_' + index + indexObs;

    if (!prop.uri) {
      return new SparqlTemplate('New Property filter', 'Please select a property to filter on');
    }

    let obsURI: string = '<' + obs.uri + '>';
    if (obs.uri === this.sessionService.doNotFilterObservation.uri) {
      obsURI = '?obsInd_' + index + indexObs;
    }

    templateString = this.setStepInd(step, templateString, index);  // même stepInd que pour FILTER_STEP
    templateString = templateString.replace(/\$obsInd/g, obsURI);  // $obsInd = variable sparql
    templateString = templateString.replace(/\$result/g, resultVar); // $result = variable sparql
    templateString = templateString.replace(/\$propInd/g, '?propInd_' + index + indexObs + indexProp); // $propInd = variable sparql
    templateString = templateString.replace(/\$propType/g, `<${prop.uri}>`); // $propType = URI de la propriété
    templateString = templateString.replace(/\$propLabel/g, propLabel); // $propLabel = variable sparql
    templateString = templateString.replace(/\$collectionInd/g, collectionVar);
    templateString = templateString.replace(/\$collectionTop/g, collectionVarTop);

    this.selectVars.push(propLabel);

    if (prop.showObject) {
      templateObject = templateObject.replace(/\$result/g, resultVar);
      templateObject = templateObject.replace(/\$objectInd/g, objectVar);
      templateObject = templateObject.replace(/\$objectLabel/g, objectLabel);
      templateString = templateString.concat(templateObject);
      this.selectVars.push(objectLabel);
    }

    templateString = this.setLanguage(templateString);


    res = new SparqlTemplate('Property filter ' + indexProp, templateString);

    if (prop.includeObjects.length > 0) {
      res.subTemplates.push(this.addStepPropertyObject(step, prop.includeObjects, index, indexObs, indexProp));
    }

    // FILTER_STEP_PROP_MAT
    // if (prop.observationMaterials[0]) {
    //   res.subTemplates.push(this.addStepPropertyMaterial(step, prop.observationMaterials[0], index, indexObs, indexProp));
    // }

    // (FILTER_QUALITY               ||          FILTER_QUANTITY )
    res.subTemplates.push(
      // tslint:disable-next-line:max-line-length
        await this.addValue(prop.numValue, prop.units, prop.boundary, prop.availableUnits, (index * 100) + (indexObs * 10) + indexProp, resultVar, 'prop'));

    return res;
  }

  private async addMaterialParameterFilter(material: Filter, index: number): Promise<SparqlTemplate> {
    let templateString = FILTER_STEP_MAT_PARAM;
    const resultVar = '?paramResult_' + index;
    let res: SparqlTemplate;
    const propLabel = '?materialParameter_' + index;

    if (!material.measurement) {
      // templateString = templateString.replace('VALUES $propInd {$propType}.', '');
      templateString = this.addOptional(templateString);
    } else {
      templateString = templateString.replace(/\$propType/g, `<${material.measurement.uri}>`);  // $propType = URI de la propriété
    }
    templateString = this.setActuatorInd(templateString, index); // $actuatorInd = variable sparql
    templateString = this.setMaterialInd(templateString, index); // même $samplerInd que pour l'étape_materiel
    templateString = templateString.replace(/\$propIndLabel/g, propLabel); // $propIndLabel = variable sparql
    templateString = templateString.replace(/\$result/g, resultVar);
    templateString = templateString.replace(/\$propInd/g, '?propInd_' + index); // $propInd = variable sparql
    templateString = this.setLanguage(templateString);

    res = new SparqlTemplate('Material Parameters filter', templateString);

    this.selectVars.push(propLabel);

    if (material.measurement) {
      // FILTER_QUANTITY || FILTER_QUALITY
      res.subTemplates.push(await this.addValueMeasurment(material.measurement, index, resultVar, 'matParam'));
    }

    return res;
  }

  private async addComponentIngredient(component: Filter, index: number, inputOutput: string, prefix: string): Promise<SparqlTemplate> {
    let templateString = FILTER_STEP_INPUT_OUTPUT_INGR;
    let res: SparqlTemplate;
    const resultVar = '?ingredientResult_' + prefix.toLowerCase() + index;
    const ingredientLabel = '?ingredientLabel_' + prefix.toLowerCase() + index;

    if (!component.measurement) {
      templateString = templateString.replace('$ingredient rdf:type $ingredientType.', '');
      templateString = this.addOptional(templateString);
    } else {
      // $ingredientType = URI de l'ingredient selectionné
      templateString = templateString.replace('$ingredientType', `<${component.measurement.uri}>`);
    }
    // même $inputOutput que pour FILTER_STEP_INPUT_OUTPUT
    templateString = this.setinputOutput(templateString, inputOutput);
    templateString = templateString.replace(/\$ingredientLabel/g, ingredientLabel);
    // $ingredient = variable sparql
    templateString = templateString.replace(/\$ingredient/g, '?ingredient_' + prefix.toLowerCase() + index);
    // $result = variable sparql
    templateString = templateString.replace(/\$result/g, resultVar);
    templateString = this.setLanguage(templateString);

    this.selectVars.push(ingredientLabel);

    res = new SparqlTemplate(prefix + ' Component Ingredient filter', templateString);

    if (component.measurement) {
      // FILTER_QUANTITY || FILTER_QUALITY
      res.subTemplates.push(await this.addValueMeasurment(component.measurement, index, resultVar, prefix.toLowerCase() + 'Ingr'));
    }

    return res;
  }

  private addStepPropertyObject(step: Step, propOOI: ObjectOI[], index: number, indexObs: number, indexProp: number): SparqlTemplate {
    const templateString = FILTER_OBS_PROP_OBJECT;
    const fullTemplate: string[] = [];
    let res: SparqlTemplate;
    const resultVar = '?propertyResult_' + index + indexObs + indexProp;

    let indexOOI = 0;
    for (const  o of propOOI) {
      if (o) {
        const objectLabel = '?objectLabel_' + index + indexObs + indexProp + indexOOI;
        let templateStringOOI = templateString;
        templateStringOOI = templateStringOOI.replace(/\$result/g, resultVar);
        templateStringOOI = templateStringOOI.replace(/\$objectType/g, `<${o.uri}>`);
        templateStringOOI = templateStringOOI.replace(/\$objectLabel/g, objectLabel);
        templateStringOOI = this.setLanguage(templateStringOOI);

        this.selectVars.push(objectLabel);
        fullTemplate.push(templateStringOOI);

        indexOOI++;
      }
    }
    res = new SparqlTemplate('Object of interest filter ' + indexProp, fullTemplate.join('\n\n'));
    return res;
  }

  // tslint:disable-next-line:max-line-length
  private addObservationMaterial(step: Step, obsMat: ObservationMaterial, index: number, indexObs: number, indexProp: number): SparqlTemplate {
    let templateString = FILTER_STEP_PROP_MAT;
    let res: SparqlTemplate;
    const sensorLabel = '?observationMaterial_' + index + indexObs + indexProp;

    if (!obsMat) {
      templateString = templateString.replace('$sensorInd rdf:type $sensorType.', '');
      templateString = this.addOptional(templateString);
    } else {
      // $sensorType = URI du materiel
      templateString = templateString.replace(/\$sensorType/g, `<${obsMat.uri}>`);
    }
    // même $obsInd que pour FILTER_STEP_PROP
    templateString = templateString.replace(/\$obsInd/g, '?obsInd_' + index + indexObs);
    // $sensorIndLabel = variable sparql
    templateString = templateString.replace(/\$sensorIndLabel/g, sensorLabel);
    // $sensorInd = variable sparql
    templateString = templateString.replace(/\$sensorInd/g, '?sensorInd_' + index + indexObs + indexProp);
    templateString = this.setLanguage(templateString);

    this.selectVars.push(sensorLabel);

    res = new SparqlTemplate('Observation material filter', templateString);

    return res;
  }

  private addOptional(templateString: string): string {
    const lb = `
`;
    return templateString = lb + 'optional {' + templateString + '}';
  }

  private async addValueMeasurment(measurement: Measurement, index: number, result: string, type: string): Promise<SparqlTemplate> {
    return await this.addValue(measurement.value, measurement.unit, measurement.boundary, measurement.availableUnits, index, result, type);
  }

  // tslint:disable-next-line:max-line-length
  private async addValue(value: string, unit: string, boundary: string, avUnit: string[], index: number, result: string, type: string): Promise<SparqlTemplate> {
    let res: SparqlTemplate;
    if (avUnit.length > 0) {
      res = await this.addQuantityValue(value, boundary, index, result, type, unit);
    } else {
      res = this.addQualityValue(value, index, result, type);
    }
    return res;
  }

  private addQualityValue(value: string, index: number, result: string, type: string): SparqlTemplate {
    let templateString: string;
    templateString = FILTER_QUALITY;
    let res: SparqlTemplate;
    const valueVar = '?qualitativeValue_' + type + '_' + index;

    // même result que FILTER_STEP_INPUT_OUTPUT_INGR || FILTER_STEP_PROP || FILTER_STEP_MAT_PARAM
    templateString = templateString.replace(/\$result/g, result);
    // $value = variable sparql
    templateString = templateString.replace(/\$value/g, valueVar);
    templateString = this.setLanguage(templateString);

    this.selectVars.push(valueVar);

    res = new SparqlTemplate('Quality filter', templateString);

    // FILTER_QUALITY_FILTER
    if (value) {
      res.subTemplates.push(this.addQualityValueFilter(value, index, type));
    }

    return res;
  }

  // tslint:disable-next-line:max-line-length
  private async addQuantityValue(value: string, boundary: string, index: number, result: string, type: string, unitLabelValue: string): Promise<SparqlTemplate> {
    let templateString: string;
    templateString = FILTER_QUANTITY;
    let res: SparqlTemplate;
    const valueMaxVar = '?valueMax_' + type + '_' + index;
    const valueMinVar = '?valueMin_' + type + '_' + index;
    const unitLabel = '?unitLabel_' + type + '_' + index;
    const unitOrigin = '?unitOrigin_' + type + '_' + index;
    const valueOrigin = '?valueOrigin_' + type + '_' + index;


    // même result que FILTER_STEP_INPUT_OUTPUT_INGR || FILTER_STEP_PROP || FILTER_STEP_MAT_PARAM
    templateString = templateString.replace(/\$result/g, result);
    // $valueMax = variable sparql
    templateString = templateString.replace(/\$valueMax/g, valueMaxVar);
    // $valueMin = variable sparql
    templateString = templateString.replace(/\$valueMin/g, valueMinVar);
    templateString = templateString.replace(/\$unitLabel/g, unitLabel);

    templateString = templateString.replace(/\$unitOrigin/g, unitOrigin);
    templateString = templateString.replace(/\$valueOrigin/g, valueOrigin);

    // templateString = templateString.replace(/\$unit/g, '?unit_' + type + index);
    templateString = this.setLanguage(templateString);

    this.selectVars.push(valueOrigin);
    this.selectVars.push(unitOrigin);

    res = new SparqlTemplate('Quantity filter', templateString);

    // FILTER_QUANTITY_EXACT || FILTER_QUANTITY_MIN || FILTER_QUANTITY_MAX
    if (value) {
      // conversion des valeurs dans l'unité de base
        res.subTemplates.push(await this.addQuantityValueFilter(value, index, boundary, type, unitLabel, unitLabelValue));
    }

    return res;
  }

  // tslint:disable-next-line:max-line-length
   private async addQuantityValueFilter(value: string, index: number, boundary: string, type: string, unitLabel: string, unitLabelValue: string): Promise<SparqlTemplate> {
    let templateString: string;
    const res = new SparqlTemplate('Quantity value filter', '');
    if (boundary === 'exact') {
      templateString = FILTER_QUANTITY_EXACT;
    } else if (boundary === 'min') {
      templateString = FILTER_QUANTITY_MIN;
    } else {
      templateString = FILTER_QUANTITY_MAX;
    }
    const valueMaxVar = '?valueMax_' + type + '_' + index;
    const valueMinVar = '?valueMin_' + type + '_' + index;

    let baseUnit = {
      unit: unitLabelValue,
      value
    };
    try {
      baseUnit = await this.initDataService.getConvertedUnit(unitLabelValue, value);
    } catch (e) {
      // unable to convert unit
    }

    templateString = templateString.replace(/\$valueMin/g, valueMinVar); // $valueMax & $valueMin = variable sparql
    templateString = templateString.replace(/\$valueMax/g, valueMaxVar); // $valueMax & $valueMin = variable sparql
    templateString = templateString.replace(/\$unitLabel/g, unitLabel);
    templateString = templateString.replace(/\$unitValue/g, baseUnit.unit);
    templateString = templateString.replace(/\$val/g, baseUnit.value);  // $val valeur saisie
    templateString = this.setLanguage(templateString);

    res.sparql = templateString;

    return res;
  }

  private addQualityValueFilter(value: string, index: number, type: string): SparqlTemplate {
    let templateString: string;
    templateString = FILTER_QUALITY_FILTER;
    let res: SparqlTemplate;
    const valueVar = '?qualitativeValue_' + type + '_' + index;

    // PERTINENCE ????
    templateString = templateString.replace(/\$value/g, valueVar); // $value = variable sparql
    templateString = templateString.replace(/\$val/g, value);  // $val valeur saisie
    templateString = this.setLanguage(templateString);

    this.selectVars.push(valueVar);

    res = new SparqlTemplate('Quality value filter', templateString);

    return res;
  }

  private setinputOutput(templateString: string, inputOutput: string): string {
    return templateString.replace(/\$inputOutput/g, inputOutput);
  }

  private setStepInd(step: Step, templateString: string, index: number): string {
    return templateString.replace(/\$stepInd/g, (step.doNotFilter || !step.name) ? '?stepURI_' + index : `<${step.uri}>`);
  }

  private setActuatorInd(templateString: string, index: number): string {
    return templateString.replace(/\$actuatorInd/g, '?actuatorInd_' + index);
  }
  private setMaterialInd(templateString: string, index: number): string {
    return templateString.replace(/\$materialInd/g, '?materialInd_' + index);
  }

  private setLanguage(templateString: string): string {
    return templateString.replace(/\$lang/g, this.sessionService.language);
  }

  async getSparqlQuery(): Promise<string> {
    const parts: SparqlTemplate[] = await this.generateTemplates();
    const frame = parts.splice(0, 1)[0];
    let req = '';

    req = this.addQueries(req, parts);

    req = frame.sparql.replace('[...]', req);

    return req;
  }

  private printSelectVars(): string {
    let res = '';

    for (let i = 0; i < this.selectVars.length; i++) {
      res += this.selectVars[i];
      if (i < this.selectVars.length - 1) {
        res += ' ';
      }
    }

    return res;
  }

  private addQueries(req: string, templates: SparqlTemplate[]): string {
    templates.forEach(e => {
      req = req + `
      ` + '#####' + e.title + e.sparql;
      req = this.addQueries(req, e.subTemplates || []);
    });
    return req;
  }
}
