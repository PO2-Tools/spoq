import { TestBed } from '@angular/core/testing';

import { SparqlEditorService } from './sparql-editor.service';

describe('SparqlEditorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SparqlEditorService = TestBed.get(SparqlEditorService);
    expect(service).toBeTruthy();
  });
});
