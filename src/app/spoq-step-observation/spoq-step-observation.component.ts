import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Property} from '../model/Property';
import {Step} from '../model/Step';
import {Observation} from '../model/Observation';
import {ObservationMaterial} from '../model/ObservationMaterial';
import {Process} from '../model/Process';

@Component({
  selector: 'app-spoq-step-observation',
  templateUrl: './spoq-step-observation.component.html',
  styleUrls: ['./spoq-step-observation.component.css']
})
export class SpoqStepObservationComponent implements OnInit {

  @Input() selectedProcess: Process;
  @Input() selectedStep: Step;
  @Input() observation: Observation;
  @Input() stepFormData: Step;

  @Input() selectedObsMaterial: ObservationMaterial;
  @Input() obsMaterialFormData: ObservationMaterial[];
  @Output() selectedObsMaterialChange = new EventEmitter<ObservationMaterial>();

  selectedFormObsMaterial: ObservationMaterial;
  doNotFilterObsMat: ObservationMaterial;

  @Output() close = new EventEmitter<boolean>();

  selectedObservationTitle: string;

  constructor() { }

  closeCard(): void {
    this.close.emit(true);
  }

  ngOnInit() {
    this.doNotFilterObsMat = new ObservationMaterial('doNotFilter', '');
    if (this.selectedObsMaterial) {
      this.selectedFormObsMaterial = new ObservationMaterial(this.selectedObsMaterial.name, this.selectedObsMaterial.uri);
      // Not implemented yet, do not use
      /*this.initDataService.getObsMaterialParams(this.selectedProcessUri, this.selectedStepUri, this.selectedFormObsMaterial)
        .subscribe(params =>
          this.selectedFormObsMaterial.obsContParam = params
        );*/
    }
  }

  addProperty() {
    this.observation.properties.push(new Property(null, null));
  }

  closeProperty($event: boolean, index: number) {
    this.observation.properties.splice(index, 1);
  }

  obsMaterialSelected(component: ObservationMaterial): void {
    this.selectedFormObsMaterial = component;
    if (component == null) {
      this.selectedObsMaterial = null;
      this.selectedObsMaterialChange.emit(this.selectedObsMaterial);
      return;
    }
    this.selectedObsMaterial = new ObservationMaterial(component.name, component.uri);
    this.selectedObsMaterial.obsContParam.push(null);
    this.selectedObsMaterialChange.emit(this.selectedObsMaterial);
    // Not implemented yet, do not use
    /*this.initDataService.getObsMaterialParams(this.selectedProcessUri, this.selectedStepUri, component).subscribe(params =>
      component.obsContParam = params
    );*/
    console.log(this.selectedObsMaterial);
  }

  compareObjects(o1: any, o2: any): boolean {
    if (o1 == null || o2 == null) {
      return false;
    }
    return o1.name === o2.name;
  }
}
