import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqStepObservationComponent } from './spoq-step-observation.component';

describe('SpoqStepObservationComponent', () => {
  let component: SpoqStepObservationComponent;
  let fixture: ComponentFixture<SpoqStepObservationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqStepObservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqStepObservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
