import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqFormComponent } from './spoq-form.component';

describe('SpoqFormComponent', () => {
  let component: SpoqFormComponent;
  let fixture: ComponentFixture<SpoqFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
