import { Component, OnInit, OnDestroy } from '@angular/core';

import { Dataset } from '../model/Dataset';
import { InitDataService } from '../services/init-data.service';
import { Process } from '../model/Process';
import { Step } from '../model/Step';
import { SessionService } from '../services/session.service';
import { FormParameters } from '../model/FormParameters';
import { SpoqWarningDialogComponent } from '../spoq-warning-dialog/spoq-warning-dialog.component';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ExecuteRequestService } from '../services/execute-request.service';
import { Router } from '@angular/router';
import {DatasetSelectionComponent} from '../dataset-selection/dataset-selection.component';
import {ProcessSelectionComponent} from '../process-selection/process-selection.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import {forkJoin, Observable} from 'rxjs';
import {StepSelectionComponent} from '../step-selection/step-selection.component';

@Component({
  selector: 'app-spoq-form',
  templateUrl: './spoq-form.component.html',
  styleUrls: ['./spoq-form.component.css']
})
export class SpoqFormComponent implements OnInit, OnDestroy {

  previouslySelectedDatasets: Dataset[];

  constructor(
    private initDataService: InitDataService,
    public sessionsService: SessionService,
    private router: Router,
    private executeRequestService: ExecuteRequestService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  test(): void {
    console.log({
      formData: this.sessionsService.availableDatasets,
      selectedParameters: this.sessionsService.selectedParameters,
      doNotRemind: this.sessionsService.doNotRemind,
      doNotFilterProcess: this.sessionsService.doNotFilterProcess, executing: this.sessionsService.executing
    });
  }

  ngOnInit(): void {
    if (this.sessionsService.justLoaded.length) {
      this.loadForm(this.sessionsService.justLoaded[0]);
    } else if (!this.sessionsService.isCached()) {
      console.log('getInitData');
      this.initDataService.getInitData().subscribe(data => {
        this.sessionsService.availableDatasets = data;
      });
      this.initDataService.initAllUnitsMap();
    }
  }

  ngOnDestroy(): void {
    this.purgeNonInitializedParams();
  }

  canExecute(): boolean {
    let stepHasBeenInit = false;
    for (const step of this.sessionsService.selectedParameters.selectedSteps) {
      if (!stepHasBeenInit && (step.uri || step.typeUri || step.doNotFilter)) { stepHasBeenInit = true; }
    }
    if (!stepHasBeenInit) { return false; }
    if (this.sessionsService.selectedParameters.selectedSteps.length === 0) {
      return false;
    }
    if (this.sessionsService.executing || this.sessionsService.datasetsLoading.length !== 0) {
      return false;
    }
    return true;
  }

  confirmDatasetChange(): void {
    this.cacheSelectedRepos();
    const selectedProcess = this.sessionsService.selectedParameters.selectedProcess;
    const selectedSteps = this.sessionsService.selectedParameters.selectedSteps;
    const selectedDatasets = this.sessionsService.selectedParameters.selectedDatasets;
    if (this.sessionsService.doNotRemind) { return; } else
      if (selectedDatasets.length === 0) { return; } else  // if no dataset selected
        if (selectedProcess == null && selectedDatasets.length === 1) { return; } else // if no process selected (but can be selected)
          if (selectedDatasets.length > 1 && selectedSteps.length === 0) { return; } // if no process selected (but can be selected)
    // this.openDialog();
  }

  private cacheSelectedRepos(): void {
    this.previouslySelectedDatasets = this.sessionsService.selectedParameters.selectedDatasets.concat([]);
  }

  confirmProcessChange(): void {
    const selectedProcess = this.sessionsService.selectedParameters.selectedProcess;
    const selectedSteps = this.sessionsService.selectedParameters.selectedSteps;
    if (this.sessionsService.doNotRemind) { return; } else
      if (selectedProcess == null || selectedSteps.length === 0) { return; }
    // this.openDialog();
  }

  resetFormDialog(): Promise<boolean> {
    const dialogRef = this.dialog.open(SpoqWarningDialogComponent, {
      width: '350px'
    });

    return dialogRef.afterClosed().toPromise();

    // subscribe(result => {
    //   if (!result) { return; }
    //     return result.keep_form;
    // });
  }

  compareObjects(o1: any, o2: any): boolean {
    if (o1 == null || o2 == null) {
      return false;
    }
    return o1.name === o2.name;
  }

  async executeForm() {
    this.purgeNonInitializedParams();
    this.sessionsService.executing = true;
    const obs = await this.executeRequestService.executeForm(this.sessionsService.selectedParameters);
    obs.subscribe(executed => {
      this.sessionsService.executing = false;
      if (!executed) {
        this.sessionsService.executing = false;
        alert('An error occured');
      } else {
        this.proceed(this.sessionsService.results.length);
      }
    });
  }

  private proceed(nbOfResults: number) {
    if (nbOfResults > 0) {
      this.router.navigate(['/results']);
    } else {
      this.snackBar.open('No results returned, please update your form settings', 'Ok', {
        duration: 10000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
    }
  }

  private purgeNonInitializedParams(): void {
    this.purgeNonInitializedSteps();
    this.purgeNonInitializedProperties();
  }

  private purgeNonInitializedSteps(): void {
    const indexes = [];

    for (let i = 0; i < this.sessionsService.selectedParameters.selectedSteps.length; i++) {
      const st = this.sessionsService.selectedParameters.selectedSteps[i];
      if (st.uri == null && st.typeUri == null && !st.doNotFilter) {
        indexes.push(i);
      }
    }
    for (let i = 0; i < indexes.length; i++) {
      const index = indexes[i] - i;
      this.sessionsService.selectedParameters.selectedSteps.splice(index, 1);
    }
  }

  private purgeNonInitializedProperties(): void {
    for (const step of this.sessionsService.selectedParameters.selectedSteps) {
      const indexes = [];

      for (let i = 0; i < step.properties.length; i++) {
        const prop = step.properties[i];
        if (prop.uri == null && prop.name == null) {
          indexes.push(i);
        }
      }
      console.log('indexes to be purged');
      console.log(indexes);
      for (let i = 0; i < indexes.length; i++) {
        const index = indexes[i] - i;
        step.properties.splice(index, 1);
      }
    }
  }

  async onDatasetSelectClose() {
    if (this.selectedNewRepos()) {
      if (this.sessionsService.selectedParameters.selectedSteps.length > 0) {
        const reset = await this.resetFormDialog();
        if (reset) {
          this.sessionsService.selectedParameters.selectedProcess = null;
          this.sessionsService.selectedParameters.selectedSteps = [];
        }
      }

      // tslint:disable-next-line:no-unused-expression
      this.sessionsService.datasetsLoading.push('loading');
      this.initDataService.updateRepositoryAdress(this.sessionsService.selectedParameters.selectedDatasets, true).subscribe(isUpdated => {
        console.log('repository location is update : ' + isUpdated);
        this.sessionsService.datasetsLoading.splice(0, 1);
        // let allProcess: Process[] = [];
        // const allJobs: Observable<any>[] = [];
        this.sessionsService.datasetsLoading.push('loading');
        this.initDataService.getListProcesses(this.sessionsService.selectedParameters.selectedDataset).subscribe(result => {
          console.log('all process ' + result);
          this.sessionsService.datasetsLoading.splice(0, 1);
          this.sessionsService.datasetsLoading.push('loading');
          this.initDataService.getDatasetProcessTree(result).then(processTree => {
            console.log('tree done');
            this.sessionsService.availableProcessTree = processTree;
            console.log(processTree);
            this.sessionsService.datasetsLoading.splice(0, 1);
          });
        });
      });
    }
  }

  selectedNewRepos(): boolean {
    if (this.sessionsService.selectedParameters.selectedDatasets.length !== this.previouslySelectedDatasets.length) {
      return true;
    }
    for (const newDts of this.sessionsService.selectedParameters.selectedDatasets) {
      if (this.previouslySelectedDatasets.findIndex(oldDts => oldDts.name === newDts.name) === -1) {
        return true;
      }
    }
    return false;
  }

  async processSelected() {
    if (this.sessionsService.selectedParameters.selectedSteps.length > 0) {
      const reset = await this.resetFormDialog();
      if (reset) {
        this.sessionsService.selectedParameters.selectedSteps = [];
      }
    }

    this.sessionsService.datasetsLoading.push('loading');
    this.initDataService
      .getProcessesSteps(this.sessionsService.selectedParameters.selectedProcess, !this.sessionsService.selectedParameters.selectedProcess.uri)
      .then(value => {
        this.sessionsService.datasetsLoading.splice(0, 1);
    });
    // }
  }

  addStep(): void {
    this.sessionsService.selectedParameters.selectedSteps.push(new Step(null, null, null, null));
    console.log('adding new empty step');
  }

  closeStep(close: boolean, index: number): void {
    this.sessionsService.selectedParameters.selectedSteps.splice(index, 1);
  }

  loadForm(success: boolean): void {
    console.log('loadForm');
    this.sessionsService.justLoaded.splice(0, 1);
    if (!success) {
      alert('Error while loading selected save, please try again');
      return;
    }
  }

  onDatasetSelection() {
    console.log('show selection dialog');
    // caching dataset selection
    this.cacheSelectedRepos();
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.height = '45vh';
    dialogConfig.width = '40vw';
    dialogConfig.data = this.sessionsService.availableDatasets;
    const dialogRef = this.dialog.open(DatasetSelectionComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog dataset selection closed`);
      this.onDatasetSelectClose();
    });
  }

  onProcessSelection() {
    console.log('show selection process dialog');
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.height = '60vh';
    dialogConfig.width = '50vw';
    dialogConfig.data = this.sessionsService.availableProcessTree;
    const dialogRef = this.dialog.open(ProcessSelectionComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog select process closed`, result);
      this.processSelected();
    });
  }
}
