import {Component, Inject, Input, OnInit} from '@angular/core';
import {SessionService} from '../services/session.service';
import {Process} from '../model/Process';
import {FlatTreeControl} from '@angular/cdk/tree';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';

interface FlatProcessNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-process-selection',
  templateUrl: './process-selection.component.html',
  styleUrls: ['./process-selection.component.css']
})

export class ProcessSelectionComponent implements OnInit {

  processSelected: Process;
  treeControl = new FlatTreeControl<FlatProcessNode>(node => node.level, node => node.expandable);

  private transformer = (node: Process, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.typeName,
      level,
      process: node
    };
  }

  treeFlattener = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(public sessionsService: SessionService, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.dataSource.data = data;
  }

  hasChild = (_: number, node: FlatProcessNode) => node.expandable;
  ngOnInit() {
  }

}
