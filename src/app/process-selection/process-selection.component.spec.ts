import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcessSelectionComponent } from './process-selection.component';

describe('ProcessSelectionComponent', () => {
  let component: ProcessSelectionComponent;
  let fixture: ComponentFixture<ProcessSelectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
