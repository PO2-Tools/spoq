import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatRadioModule } from '@angular/material/radio';
import { MatTreeModule } from '@angular/material/tree';
import { CopyClipboardModule } from './copy-clipboard.module';

import { AppComponent } from './app.component';
import { SpoqHeaderComponent } from './spoq-header/spoq-header.component';
import { SpoqFormComponent } from './spoq-form/spoq-form.component';
import { SpoqResultsPageComponent } from './spoq-results-page/spoq-results-page.component';
import { SpoqFormStepComponent } from './spoq-form-step/spoq-form-step.component';
import { SpoqStepComponentComponent } from './spoq-step-filter/spoq-step-filter.component';
import { SpoqStepPropertyComponent } from './spoq-step-property/spoq-step-property.component';
import { SpoqSparqlPageComponent } from './spoq-sparql-page/spoq-sparql-page.component';
import { SpoqSaveDialogComponent } from './spoq-save-dialog/spoq-save-dialog.component';
import { SpoqLoadDialogComponent } from './spoq-load-dialog/spoq-load-dialog.component';
import { SpoqWarningDialogComponent } from './spoq-warning-dialog/spoq-warning-dialog.component';
import { SpoqFormatOptionsComponent } from './spoq-format-options/spoq-format-options.component';
import { SpoqPropertyFilterComponent } from './spoq-property-filter/spoq-property-filter.component';
import { SpoqSparqlTemplateComponent } from './spoq-sparql-template/spoq-sparql-template.component';
import { SpoqSparqlTemplateCardComponent } from './spoq-sparql-template-card/spoq-sparql-template-card.component';
import { SpoqSparqlTemplateCardDialogComponent } from './spoq-sparql-template-card-dialog/spoq-sparql-template-card-dialog.component';
import { SpoqSparqlErrorMessageComponent } from './spoq-sparql-error-message/spoq-sparql-error-message.component';
import { SpoqLoadingBarComponent } from './spoq-loading-bar/spoq-loading-bar.component';
import { SpoqLoginPageComponent } from './spoq-login-page/spoq-login-page.component';
import { DatasetSelectionComponent } from './dataset-selection/dataset-selection.component';
import { ProcessSelectionComponent } from './process-selection/process-selection.component';
import { StepSelectionComponent } from './step-selection/step-selection.component';

import { CodemirrorModule } from '@ctrl/ngx-codemirror';
import { MyHttpInterceptor} from './services/MyHttpInterceptor';
import { ObservationSelectionComponent } from './observation-selection/observation-selection.component';
import { SpoqStepObservationComponent } from './spoq-step-observation/spoq-step-observation.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { UnitToolComponent } from './unit-tool/unit-tool.component';
import { PublishDataverseComponent } from './publish-dataverse/publish-dataverse.component';
import { SpoqLoadDatasetDialogComponent } from './spoq-load-dataset-dialog/spoq-load-dataset-dialog.component';


@NgModule({
    declarations: [
        AppComponent,
        SpoqHeaderComponent,
        SpoqFormComponent,
        SpoqResultsPageComponent,
        SpoqFormStepComponent,
        SpoqStepComponentComponent,
        SpoqStepPropertyComponent,
        SpoqSparqlPageComponent,
        SpoqSaveDialogComponent,
        SpoqLoadDialogComponent,
        SpoqWarningDialogComponent,
        SpoqFormatOptionsComponent,
        SpoqPropertyFilterComponent,
        SpoqSparqlTemplateComponent,
        SpoqSparqlTemplateCardComponent,
        SpoqSparqlTemplateCardDialogComponent,
        SpoqSparqlErrorMessageComponent,
        SpoqLoadingBarComponent,
        SpoqLoginPageComponent,
        DatasetSelectionComponent,
        ProcessSelectionComponent,
        StepSelectionComponent,
        ObservationSelectionComponent,
        SpoqStepObservationComponent,
        UnitToolComponent,
        PublishDataverseComponent,
        SpoqLoadDatasetDialogComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MaterialModule,
        FormsModule,
        CopyClipboardModule,
        HttpClientModule,
        MatDialogModule,
        MatGridListModule,
        MatTreeModule,
        MatRadioModule,
        CodemirrorModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        MatSidenavModule,
        MatSnackBarModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: MyHttpInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
