import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Step } from '../model/Step';
import { InitDataService } from '../services/init-data.service';
import { Property } from '../model/Property';
import { Dataset } from '../model/Dataset';
import { Process } from '../model/Process';
import { SessionService } from '../services/session.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {DatasetSelectionComponent} from '../dataset-selection/dataset-selection.component';
import {StepSelectionComponent} from '../step-selection/step-selection.component';
import {ObservationSelectionComponent} from '../observation-selection/observation-selection.component';
import {Observation} from '../model/Observation';
import {Filter} from '../model/Filter';


@Component({
  selector: 'app-spoq-form-step',
  templateUrl: './spoq-form-step.component.html',
  styleUrls: ['./spoq-form-step.component.css']
})
export class SpoqFormStepComponent implements OnInit {

  constructor(
    public initDataService: InitDataService,
    public dialog: MatDialog,
    public sessionService: SessionService
  ) { }
  @Output() close = new EventEmitter<boolean>();
  @Input() selectedStep: Step;
  doNotFilter: Step;
  selectedProcess: Process;
  advancedUser: boolean;
  isTemplateCardShown: boolean;

    protected readonly Filter = Filter;

  ngOnInit() {
    this.doNotFilter = this.getDoNotFilter();
    this.selectedProcess = this.sessionService.selectedParameters.selectedProcess;
    this.advancedUser = this.sessionService.advancedUser;
    this.isTemplateCardShown = this.sessionService.isTemplateCardShown;
    if (this.selectedStep.uri || this.selectedStep.typeUri) {
      this.getStep(this.selectedStep);
    }
  }

  observationSelection(obs: Observation) {
    const selectedObs = new Observation(obs.name, obs.uri);
    this.initDataService.getStepObservationProperty(this.selectedProcess, this.selectedStep, selectedObs)
      .subscribe(res => selectedObs.availableProperties = res);
    this.initDataService.getStepObservationMaterial(this.selectedProcess, this.selectedStep, selectedObs)
        .subscribe(res => selectedObs.availableMaterial = res);
    this.selectedStep.observations.push(selectedObs);
  }

  stepSelected(step: Step): void {
    // clonage de la step pour ne pas perdre la référence !
    this.selectedStep.copyFrom(step);
    this.getStep(this.selectedStep);

  }

  private getStep(step: Step) {
    this.initDataService.getStepMaterials(this.selectedProcess, step)
        .subscribe(res => step.filterAvailable[Filter.MATERIAL] = res);
    this.initDataService.getStepInputComponent(this.selectedProcess, step)
        .subscribe(res => step.filterAvailable[Filter.INPUTCOMPONENT] = res);
    this.initDataService.getStepOutputComponent(this.selectedProcess, step)
        .subscribe(res => step.filterAvailable[Filter.OUTPUTCOMPONENT] = res);
    this.initDataService.getStepObservations(this.selectedProcess, step)
        .subscribe(res => step.observationsAvailable = res);
  }

  isStepName(title: string): boolean {
    return this.selectedProcess.steps.findIndex((element) => element.name === title) !== -1;
  }

  addProperty(): void {
    this.selectedStep.properties.push(new Property(null, null));
  }

  closeStep(): void {
    this.close.emit(true);
  }

  closeObservation(close: boolean, index: number): void {
    this.selectedStep.observations.splice(index, 1);
  }

  closeProperty(close: boolean, index: number): void {
    this.selectedStep.properties.splice(index, 1);
  }

  getDoNotFilter(): Step {
    return new Step('http://opendata.inrae.fr/PO2/core/Step', 'doNotFilter', null, null);
  }

  onStepSelection() {
    console.log('show selection step dialog');
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.height = '60vh';
    dialogConfig.width = '50vw';
    dialogConfig.data = this.selectedProcess.stepsTree;

    const dialogRef = this.dialog.open(StepSelectionComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog step selection closed `, result);
      if (result) {
        this.stepSelected(result);
      }
    });
  }

  onObservationSelection() {
    console.log('show selection observation dialog');
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.height = '60vh';
    dialogConfig.width = '50vw';
    dialogConfig.data = this.selectedStep.observationsAvailable;
    const dialogRef = this.dialog.open(ObservationSelectionComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.observationSelection(result);
      }
      console.log(`Dialog observation selection closed `);
    });
  }
}
