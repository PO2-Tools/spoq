import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqFormStepComponent } from './spoq-form-step.component';

describe('SpoqFormStepComponent', () => {
  let component: SpoqFormStepComponent;
  let fixture: ComponentFixture<SpoqFormStepComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqFormStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqFormStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
