import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqHeaderComponent } from './spoq-header.component';

describe('SpoqHeaderComponent', () => {
  let component: SpoqHeaderComponent;
  let fixture: ComponentFixture<SpoqHeaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
