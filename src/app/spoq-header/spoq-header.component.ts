import { Component, OnInit } from '@angular/core';
import { SessionService } from '../services/session.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { InitDataService } from '../services/init-data.service';
import {SpoqLoginPageComponent} from '../spoq-login-page/spoq-login-page.component';
import {UnitToolComponent} from '../unit-tool/unit-tool.component';
import {SpoqSaveDialogComponent} from '../spoq-save-dialog/spoq-save-dialog.component';
import {SpoqLoadDialogComponent} from '../spoq-load-dialog/spoq-load-dialog.component';
import {SaveLoadService} from '../services/save-load.service';
import {catchError, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs';
import {PublishDataverseComponent} from '../publish-dataverse/publish-dataverse.component';
import {SpoqLoadDatasetDialogComponent} from '../spoq-load-dataset-dialog/spoq-load-dataset-dialog.component';

@Component({
  selector: 'app-spoq-header',
  templateUrl: './spoq-header.component.html',
  styleUrls: ['./spoq-header.component.css']
})
export class SpoqHeaderComponent implements OnInit {
  title = 'Simple PO² Query';
  selectedTab: string;

  switchMode(): void {
    this.sessionsService.switchMode();
  }

  unitTool(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.height = '36vh';
    dialogConfig.width = '30vw';
    const dialogRef = this.dialog.open(UnitToolComponent, dialogConfig);
  }

  publishTool(): void {
  const dialogConfig = new MatDialogConfig();
  dialogConfig.autoFocus = true;
  dialogConfig.disableClose = true;
  dialogConfig.height = '55vh';
  dialogConfig.width = '40vw';
  const dialogRef = this.dialog.open(PublishDataverseComponent, dialogConfig);
}

  constructor(
    public sessionsService: SessionService,
    private router: Router,
    private initDataService: InitDataService,
    private saveService: SaveLoadService,
    public dialog: MatDialog,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.selectedTab = window.location.href.split('/')[3];
  }

  openLoginPage(): void {
  //  this.router.navigate(['/login']);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = '30vw';
    const dialogRef = this.dialog.open(SpoqLoginPageComponent, dialogConfig);
  }

  openSaveDialog(): void {
    const session = this.sessionsService;
    const dialogRef = this.dialog.open(SpoqSaveDialogComponent, {
      width: '40vw',
      data: {
        name: '', isAdvanced: session.isAdvancedUser()
      }
    });
  }

  startTest(): void {
    this.http.post<boolean>('http://localhost:8080/tesRetour', {}).pipe(
    catchError(err => {
      return of(true); // true pour indiquer qu'il y a une erreur.
    }),
    map(a => {
      return !a;
    })).subscribe(value => {
      console.log('value : ' + value);
    });
  }

  openLoadDialog(): void {
    const list: string[][] = [];

    this.saveService.getSaveList(this.sessionsService.isAdvancedUser()).subscribe(res => list.push(...res));

    const dialogRef = this.dialog.open(SpoqLoadDialogComponent, {
      // height: '45vh',
      width: '40vw',
      data: {saveList: list}
    });
  }

  openLoadPublish(): void {
    const list: string[] = [];

    this.saveService.getDatasetList().subscribe(res => {
      list.push(...res);
    } );

    const dialogRef = this.dialog.open(SpoqLoadDatasetDialogComponent, {
      // height: '45vh',
      width: '40vw',
      data: {saveList: list}
    });
  }

  logout(): void {
    this.sessionsService.logout().subscribe(loggedOut => {
      if (loggedOut) {
        this.sessionsService.loggedIn = !loggedOut;
        this.initDataService.getInitData().subscribe(data => {
          this.sessionsService.reset();
          this.sessionsService.availableDatasets = data;
        });
      } else {
        alert('Failed to logout, please try again later');
      }
    });
  }
}
