/* example of how I intend to store and use queries */
export const EXAMPLE_QUERY = `
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX PO2-Domain: <http://opendata.inrae.fr/PO2/Ontology/PO2_biorefinery/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX time: <http://www.w3.org/2006/time#>

select ?process_label ?step1_label ?obs1_label ?result1_label ?result2_label ?interest1_label ?attr_mat1 where {
###### process ######
	?process rdfs:subClassOf PO2:Transformation_Process.
    ?process rdfs:label ?process_label.
    ?process rdfs:label "$processLabel". ### filtrage process label;

}`;
// then I use it by doing: EXAMPLE_QUERY.replace('$processLabel', name);

/***********/

// getDatasetProcesses
// pas d'entrée. Le/Les Datasets sont implicites (on interroge un repository qui ne contient uniquement que les datasets à interroger)
export const DATASET_PROCESSES_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>

select distinct ?process_type ?process_type_label  ?process_instance ?process_instance_label where {
    ?process_instance rdfs:subClassOf PO2:Process .
    ?process_instance rdf:type <http://www.w3.org/ns/dcat#Dataset>.
    ?process_instance rdfs:subClassOf ?process_type.
    FILTER NOT EXISTS {
        ?process_instance rdfs:subClassOf ?process_inter.
        ?process_inter rdfs:subClassOf ?process_type.
    }
    ?process_type skos:prefLabel ?process_type_label .
    FILTER (langMatches( lang(?process_type_label),"$lang" ) || langMatches( lang(?process_type_label), "" )).
    ?process_instance skos:prefLabel ?process_instance_label .
    FILTER (langMatches( lang(?process_instance_label), "$lang" ) || langMatches( lang(?process_instance_label), "" )).
}`;

// + param : $processURI.
// Si on filtre sur le process alors $processURI = l'uri du process selectionné
// sinon $processURI = http://opendata.inrae.fr/PO2/core/Transformation_Process (l'utilisateur selectionne "Do not filter")
export const PROCESS_STEPS_TYPES_LABEL_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>

select distinct ?stepi ?stepi_label ?step_type ?step_type_label where {
        ?itinerary rdf:type <$processURI> .
        ?itinerary PO2:hasStep ?stepi .
        ?stepi skos:prefLabel ?stepi_label .

        ?stepi rdf:type ?step_type.
        FILTER NOT EXISTS {
            ?stepi rdf:type ?type_inter .
            ?type_inter rdfs:subClassOf ?step_type .
        }

        ?step_type skos:prefLabel ?step_type_label .
        FILTER (langMatches( lang(?stepi_label), "$lang" ) || langMatches( lang(?stepi_label), "" )).
        FILTER (langMatches( lang(?step_type_label), "$lang" ) || langMatches( lang(?step_type_label), "" )).
}`;

// + param : $sampleURI.
// sinon $processURI = http://opendata.inrae.fr/PO2/core/Transformation_Process (l'utilisateur selectionne "Do not filter")
export const SAMPLE_STEPS_TYPES_LABEL_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>

select distinct ?stepi ?stepi_label ?step_type ?step_type_label where {
        ?process PO2:sampleName <$sampleURI> .
        ?iti rdf:type ?process .
        ?iti PO2:hasForStep ?stepi .
        ?stepi skos:prefLabel ?stepi_label .

        ?stepi rdf:type ?step_type.
        FILTER NOT EXISTS {
            ?stepi rdf:type ?type_inter .
            ?type_inter rdfs:subClassOf ?step_type .
        }

        ?step_type skos:prefLabel ?step_type_label .
        FILTER (langMatches( lang(?stepi_label), "$lang" ) || langMatches( lang(?stepi_label), "" )).
        FILTER (langMatches( lang(?step_type_label), "$lang" ) || langMatches( lang(?step_type_label), "" )).
}`;

// + param : $processURI
// Si on filtre sur le process alors $processURI = l'uri du process selectionné.
// Sinon $processURI = "http://opendata.inrae.fr/PO2/core/Transformation_Process"
// + param : $stepURI
// Si on filtre sur l'étape (type ou label) alors $stepURI = l'uri de l'étape (type)
// Sinon $stepURI = "http://opendata.inrae.fr/PO2/core/step"
export const STEP_MAT_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX ssn: <http://www.w3.org/ns/ssn/>


select distinct ?material_type ?material_label where {
    ?itinerary rdf:type <$processURI> .
    {
        ?itinerary PO2:hasStep <$stepURI> .
        <$stepURI> sosa:madeByActuator ?actuator.
        ?material ssn:hasSubSystem ?actuator .
    }
    UNION
    {
        ?itinerary PO2:hasStep ?step_i .
        ?step_i rdf:type <$stepURI> .
        ?step_i sosa:madeByActuator ?actuator.
        ?material ssn:hasSubSystem ?actuator .
    }

    ?material rdf:type ?material_type.
    FILTER NOT EXISTS {
        ?material rdf:type ?type_inter .
        ?type_inter rdfs:subClassOf ?material_type .
    }

    ?material_type skos:prefLabel ?material_label .
    FILTER (langMatches( lang(?material_label), "$lang" ) || langMatches( lang(?material_label), "" )).
}`;

// + param : $processURI
// Si on filtre sur le process alors $processURI = l'uri du process selectionné.
// Sinon $processURI = "http://opendata.inrae.fr/PO2/core/Transformation_Process"
// + param : $stepURI
// Si on filtre sur l'étape (type ou label) alors $stepURI = l'uri de l'étape
// Sinon $stepURI = "hhttp://opendata.inrae.fr/PO2/core/step"
export const STEP_INPUT_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>

select distinct ?input_type ?input_label where {
    ?process rdf:type <$processURI> .
    {
        ?process PO2:hasStep <$stepURI> .
        <$stepURI> PO2:hasInput ?input .
    }
    UNION
    {
        ?process PO2:hasStep ?step_i .
        ?step_i rdf:type <$stepURI> .
        ?step_i PO2:hasInput ?input .
    }

    ?input rdf:type ?input_type.
    FILTER NOT EXISTS {
        ?input rdf:type ?type_inter .
        ?type_inter rdfs:subClassOf ?input_type .
    }

    ?input_type skos:prefLabel ?input_label .
    FILTER (langMatches( lang(?input_label), "$lang" ) || langMatches( lang(?input_label), "" )).
}`;

// + param : $processURI
// Si on filtre sur le process alors $processURI = l'uri du process selectionné.
// Sinon $processURI = "http://opendata.inrae.fr/PO2/core/Transformation_Process"
// + param : $stepURI
// Si on filtre sur l'étape (type ou label) alors $stepURI = l'uri de l'étape
// Sinon $stepURI = "http://opendata.inrae.fr/PO2/core/step"
export const STEP_OUPUT_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>

select distinct ?output_type ?output_label where {
    ?process rdf:type <$processURI> .
    {
        ?process PO2:hasStep <$stepURI> .
        <$stepURI> PO2:hasOutput ?output .
    }
    UNION
    {
        ?process PO2:hasStep ?step_i .
        ?step_i rdf:type <$stepURI> .
        ?step_i PO2:hasOutput ?output .
    }

    ?output rdf:type ?output_type.
    FILTER NOT EXISTS {
      ?output rdf:type ?type_inter .
      ?type_inter rdfs:subClassOf ?output_type .
    }

    ?output_type skos:prefLabel ?output_label .
    FILTER (langMatches( lang(?output_label), "$lang" ) || langMatches( lang(?output_label), "" )).
}`;


export const STEP_OBS_TYPE_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX time: <http://www.w3.org/2006/time#>
PREFIX ssn: <http://www.w3.org/ns/ssn/>
PREFIX qudt: <http://qudt.org/schema/qudt/>

select distinct ?obs_ind ?obs_ind_label where {
    ?process rdf:type <$processURI> .
    {
        ?process PO2:hasStep <$stepURI> .
        <$stepURI> sosa:phenomenonTime ?inter1 .
    }
    UNION
    {
        ?process PO2:hasStep ?step_i .
        ?step_i rdf:type <$stepURI> .
        ?step_i sosa:phenomenonTime ?inter1 .
    }
    ?inter1 time:intervalContains ?interObs .
    ?obs_ind sosa:phenomenonTime ?interObs .
    ?obs_ind rdf:type PO2:Observation.

     optional {
        ?obs_ind skos:prefLabel ?obs_ind_label
        FILTER (langMatches( lang(?obs_ind_label), "$lang" ) || langMatches( lang(?obs_ind_label), "" )).
    }
}`;


// + param : $processURI
// Si on filtre sur le process alors $processURI = l'uri du process selectionné.
// Sinon $processURI = "http://opendata.inrae.fr/PO2/core/Transformation_Process"
// + param : $stepURI
// Si on filtre sur l'étape (type ou label) alors $stepURI = l'uri de l'étape
// Sinon $stepURI = "http://opendata.inrae.fr/PO2/core/step"
export const STEP_PROP_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX time: <http://www.w3.org/2006/time#>
PREFIX ssn: <http://www.w3.org/ns/ssn/>
PREFIX qudt: <http://qudt.org/schema/qudt/>
PREFIX schema: <http://schema.org/>


select distinct ?prop_type ?prop_type_label ?unit_label where {
    ?process rdf:type <$processURI> .
    $comment ?process rdf:type / PO2:sampleName $sampleURI .
    {
        ?process PO2:hasForStep <$stepURI> .
        <$stepURI> sosa:phenomenonTime ?inter1 .
    }
    UNION
    {
        ?process PO2:hasForStep ?step_i .
        ?step_i rdf:type <$stepURI> .
        ?step_i sosa:phenomenonTime ?inter1 .
    }
    ?inter1 time:intervalContains ?interObs .
    ?obs_i sosa:phenomenonTime ?interObs .
    ?obs_i (PO2:observationResult | PO2:computedResult) | (PO2:seriesResult / PO2:hasSingularMeasure) ?resulti .
    ?resulti ssn:hasProperty ?propi .

    ?propi rdf:type ?prop_type.
    FILTER NOT EXISTS {
      ?propi rdf:type ?type_inter .
      ?type_inter rdfs:subClassOf ?prop_type .
    }

    ?prop_type skos:prefLabel ?prop_type_label .
    FILTER (langMatches( lang(?prop_type_label), "$lang" ) || langMatches( lang(?prop_type_label), "" )).
    optional {
        ?resulti schema:unitText | schema:unitCode ?unit_label .
    }
}`;

// + param : $processURI
// Si on filtre sur le process alors $processURI = l'uri du process selectionné.
// Sinon $processURI = "http://opendata.inrae.fr/PO2/core/Transformation_Process"
// + param : $stepURI
// Si on filtre sur l'étape (type ou label) alors $stepURI = l'uri de l'étape
// Sinon $stepURI = "http://opendata.inrae.fr/PO2/core/step"
export const STEP_OBS_PROP_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX time: <http://www.w3.org/2006/time#>
PREFIX ssn: <http://www.w3.org/ns/ssn/>
PREFIX qudt: <http://qudt.org/schema/qudt/>
PREFIX schema: <http://schema.org/>


select distinct ?prop_type ?prop_type_label ?unit_label ?obj_label ?obj_type where {
    ?process rdf:type <$processURI> .
    {
        ?process PO2:hasStep <$stepURI> .
        <$stepURI> sosa:phenomenonTime ?inter1 .
    }
    UNION
    {
        ?process PO2:hasStep ?step_i .
        ?step_i rdf:type <$stepURI> .
        ?step_i sosa:phenomenonTime ?inter1 .
    }

    $obsURI sosa:phenomenonTime / ^time:intervalContains ?inter1 .
    $obsURI PO2:hasObservationCollection/sosa:hasMember+ ?obsSOSA.

    ?obsSOSA sosa:observedProperty ?propi.
    ?propi rdf:type ?prop_type.
    FILTER NOT EXISTS {
      ?propi rdf:type ?type_inter .
      ?type_inter rdfs:subClassOf ?prop_type .
    }

    ?prop_type skos:prefLabel ?prop_type_label .
    FILTER (langMatches( lang(?prop_type_label), "$lang" ) || langMatches( lang(?prop_type_label), "" )).
    ?obsSOSA sosa:hasResult ?resulti.
    optional {
        ?resulti schema:unitText | schema:unitCode ?unit_label .
    }

    optional {
        ?propi PO2:hasObjectOfInterest ?obj_type .
        ?obj_type skos:prefLabel ?obj_label .
        FILTER (langMatches( lang(?obj_label), "en" ) || langMatches( lang(?obj_label), "" )).
    }
}`;

// + param : $processURI
// Si on filtre sur le process alors $processURI = l'uri du process selectionné.
// Sinon $processURI = "http://opendata.inrae.fr/PO2/core/Transformation_Process"
// + param : $stepURI
// Si on filtre sur l'étape (type ou label) alors $stepURI = l'uri de l'étape
// Sinon $stepURI = "http://opendata.inrae.fr/PO2/core/step"
// + param : $outputURI URI du type de la composition
// getComponentIngredients
export const STEP_OUTPUT_COMPONENT_INGREDIENTS_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX qudt: <http://qudt.org/schema/qudt/>
PREFIX ssn: <http://www.w3.org/ns/ssn/>
PREFIX schema: <http://schema.org/>


select distinct ?ingr_type ?ingr_type_label ?car_label ?unit_label where {
    ?itinerary rdf:type <$processURI> .
    {
        ?itinerary PO2:hasStep <$stepURI> .
        <$stepURI> PO2:hasOutput ?output .
    }
    UNION
    {
        ?itinerary PO2:hasStep ?step_i .
        ?step_i rdf:type <$stepURI> .
        ?step_i PO2:hasOutput ?output .
    }

    ?output rdf:type <$outputURI>.
    FILTER NOT EXISTS {
      ?output rdf:type ?type_inter .
      ?type_inter rdfs:subClassOf <$outputURI> .
    }

    ?output PO2:isComposedOf ?ingr .

    ?ingr rdf:type ?ingr_type.
    FILTER NOT EXISTS {
      ?ingr rdf:type ?type_inter_2 .
      ?type_inter_2 rdfs:subClassOf ?ingr_type .
    }

    ?ingr_type skos:prefLabel ?ingr_type_label .
    FILTER (langMatches( lang(?ingr_type_label), "$lang" ) || langMatches( lang(?ingr_type_label), "" )).

    ?ingr ssn:hasProperty ?prop .

    ?prop rdf:type ?prop_type.
    FILTER NOT EXISTS {
      ?prop rdf:type ?prop_type_inter .
      ?prop_type_inter rdfs:subClassOf ?prop_type .
    }
    ?prop_type skos:prefLabel ?car_label.
    FILTER (langMatches( lang(?car_label), "$lang" ) || langMatches( lang(?car_label), "" )).

    optional {
        ?prop schema:unitText | schema:unitCode ?unit_label .
    }
}`;


// + param : $processURI
// Si on filtre sur le process alors $processURI = l'uri du process selectionné.
// Sinon $processURI = "http://opendata.inrae.fr/PO2/core/Transformation_Process"
// + param : $stepURI
// Si on filtre sur l'étape (type ou label) alors $stepURI = l'uri de l'étape
// Sinon $stepURI = "http://opendata.inrae.fr/PO2/core/step"
// + param : $inputURI URI du type de la composition
export const STEP_INPUT_COMPONENT_INGREDIENTS_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX qudt: <http://qudt.org/schema/qudt/>
PREFIX ssn: <http://www.w3.org/ns/ssn/>
PREFIX schema: <http://schema.org/>


select distinct ?ingr_type ?ingr_type_label ?car_label ?unit_label where {
    ?itinerary rdf:type <$processURI> .
    {
        ?itinerary PO2:hasStep <$stepURI> .
        <$stepURI> PO2:hasInput ?input .
    }
    UNION
    {
        ?itinerary PO2:hasStep ?step_i .
        ?step_i rdf:type <$stepURI> .
        ?step_i PO2:hasInput ?input .
    }

    ?input rdf:type <$inputURI>.
    FILTER NOT EXISTS {
      ?input rdf:type ?type_inter .
      ?type_inter rdfs:subClassOf <$inputURI> .
    }

    ?input PO2:isComposedOf ?ingr .

    ?ingr rdf:type ?ingr_type.
    FILTER NOT EXISTS {
      ?ingr rdf:type ?type_inter_2 .
      ?type_inter_2 rdfs:subClassOf ?ingr_type .
    }

    ?ingr_type skos:prefLabel ?ingr_type_label .
    FILTER (langMatches( lang(?ingr_type_label), "$lang" ) || langMatches( lang(?ingr_type_label), "" )).

    ?ingr ssn:hasProperty ?prop .

    ?prop rdf:type ?prop_type.
    FILTER NOT EXISTS {
      ?prop rdf:type ?prop_type_inter .
      ?prop_type_inter rdfs:subClassOf ?prop_type .
    }
    ?prop_type skos:prefLabel ?car_label.
    FILTER (langMatches( lang(?car_label), "$lang" ) || langMatches( lang(?car_label), "" )).

    optional {
        ?prop schema:unitText | schema:unitCode ?unit_label .
    }
}`;


// + param : $processURI
// Si on filtre sur le process alors $processURI = l'uri du process selectionné.
// Sinon $processURI = "http://opendata.inrae.fr/PO2/core/Transformation_Process"
// + param : $stepURI
// Si on filtre sur l'étape (type ou label) alors $stepURI = l'uri de l'étape
// Sinon $stepURI = "http://opendata.inrae.fr/PO2/core/step"
// + param : $samplerURI URI du materiel
// getStepMaterialParams
export const STEP_MATERIALS_PARAMS_QUERY = `

PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX ssn: <http://www.w3.org/ns/ssn/>
PREFIX qudt: <http://qudt.org/schema/qudt/>
PREFIX schema: <http://schema.org/>


select distinct ?car_label ?unit_label ?car_URI where {
    ?itinerary rdf:type <$processURI> .
    {
        ?itinerary PO2:hasStep <$stepURI> .
        <$stepURI> sosa:madeByActuator ?actuator .
        ?material ssn:hasSubSystem ?actuator.
    }
    UNION
    {
        ?itinerary PO2:hasStep ?step_i .
        ?step_i rdf:type <$stepURI> .
        ?step_i sosa:madeByActuator ?actuator .
        ?material ssn:hasSubSystem ?actuator.
    }
    ?material rdf:type <$materialURI> .
    ?actuator ssn:hasProperty ?prop .

    ?prop rdf:type ?car_URI.
    FILTER NOT EXISTS {
      ?prop rdf:type ?type_inter .
      ?type_inter rdfs:subClassOf ?car_URI .
    }
    ?car_URI skos:prefLabel ?car_label .
    FILTER (langMatches( lang(?car_label), "$lang" ) || langMatches( lang(?car_label), "" )).

    optional {
        ?prop schema:unitText | schema:unitCode ?unit_label .
    }
}`;

// + param : $processURI
// Si on filtre sur le process alors $processURI = l'uri du process selectionné.
// Sinon $processURI = "http://opendata.inrae.fr/PO2/core/Transformation_Process"
// + param : $stepURI
// Si on filtre sur l'étape (type ou label) alors $stepURI = l'uri de l'étape
// Sinon $stepURI = "http://opendata.inrae.fr/PO2/core/step"
// + param : $samplerURI URI du materiel
// getObsMaterialParams
export const OBS_MATERIAL_PARAMS_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX ssn: <http://www.w3.org/ns/ssn/>
PREFIX qudt: <http://qudt.org/schema/qudt/>
PREFIX time: <http://www.w3.org/2006/time#>
PREFIX schema: <http://schema.org/>


select distinct ?car_label ?unit_label ?car_URI where {
    ?process rdf:type <$processURI> .
    {
        ?process PO2:hasForStep <$stepURI> .
        <$stepURI> sosa:phenomenonTime ?inter1 .
    }
    UNION
    {
        ?process PO2:hasForStep ?step_i .
        ?step_i rdf:type <$stepURI> .
        ?step_i sosa:phenomenonTime ?inter1 .
    }
    ?inter1 time:intervalContains ?interObs .
    ?obs_i sosa:phenomenonTime ?interObs .
    ?obs_i rdf:type <$obsURI> .
    ?obs_i sosa:madeBySensor ?sampler .
    ?sampler rdf:type <$samplerURI> .
    ?sampler PO2:hasSettingMeasure ?measure .
    ?measure ssn:hasProperty ?prop .
    ?prop skos:prefLabel ?car_label .

    ?prop rdf:type ?car_URI.
    FILTER NOT EXISTS {
      ?prop rdf:type ?type_inter .
      ?type_inter rdfs:subClassOf ?car_URI .
    }

    FILTER (langMatches( lang(?car_label), "$lang" ) || langMatches( lang(?car_label), "" )).
    optional {
        ?measure schema:unitText | schema:unitCode ?unit_label .
    }
}`;

// + param : $processURI
// Si on filtre sur le process alors $processURI = l'uri du process selectionné.
// Sinon $processURI = "http://opendata.inrae.fr/PO2/core/Transformation_Process"
// + param : $stepURI
// Si on filtre sur l'étape (type ou label) alors $stepURI = l'uri de l'étape
// Sinon $stepURI = "http://opendata.inrae.fr/PO2/core/step"
export const OBS_MAT_QUERY = `
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX ssn: <http://www.w3.org/ns/ssn/>
PREFIX qudt: <http://qudt.org/schema/qudt/>
PREFIX time: <http://www.w3.org/2006/time#>

select distinct ?sensor_type ?sensor_label where {
    ?process rdf:type <$processURI> .
    {
        ?process PO2:hasStep <$stepURI> .
        <$stepURI> sosa:phenomenonTime ?inter1 .
    }
    UNION
    {
        ?process PO2:hasStep ?step_i .
        ?step_i rdf:type <$stepURI> .
        ?step_i sosa:phenomenonTime ?inter1 .
    }
    $obsURI sosa:phenomenonTime / ^time:intervalContains ?inter1 .
    $obsURI PO2:hasObservationCollection/sosa:madeBySensor/^ssn:hasSubSystem ?obsSen.

    ?obsSen rdf:type ?sensor_type.
    FILTER NOT EXISTS {
        ?obsSen rdf:type ?inter_type.
        ?inter_type rdfs:subClassOf ?sensor_type.
    }

    ?sensor_type skos:prefLabel ?sensor_label .
    FILTER (langMatches( lang(?sensor_label), "$lang" ) || langMatches( lang(?sensor_label), "" )).
}`;

// getDatasetProcesses
// pas d'entrée. Le/Les Datasets sont implicites (on interroge un repository qui ne contient uniquement que les datasets à interroger)
export const TEMPLATE_OBJECT_HIERARCHIE = `
{
    <$objectURI> rdfs:subClassOf* ?son.
}
`;

export const OBJECT_FILTERED_HIERARCHIE = `
  PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
  PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
  PREFIX sesame: <http://www.openrdf.org/schema/sesame#>

  select distinct ?father ?fatherLabel ?son ?sonLabel where {
    $TEMPLATE_OBJECT_HIERARCHIE

    ?son rdfs:subClassOf ?father.
    FILTER NOT EXISTS {
        ?son rdfs:subClassOf ?inter_type.
        ?inter_type rdfs:subClassOf ?father.
    }
    ?father skos:prefLabel ?fatherLabel.
    FILTER (langMatches( lang(?fatherLabel), "$lang" ) || langMatches( lang(?fatherLabel), "" )).
    ?son skos:prefLabel ?sonLabel.
    FILTER (langMatches( lang(?sonLabel), "$lang" ) || langMatches( lang(?sonLabel), "" )).
}
`;

export const OBJECT_FULL_HIERARCHIE = `
  PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
  PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
  PREFIX sesame: <http://www.openrdf.org/schema/sesame#>

  select distinct ?father ?fatherLabel ?son ?sonLabel where {
    ?son rdfs:subClassOf* <$topURI>.
    ?father rdfs:subClassOf* <$topURI>.
    ?son rdfs:subClassOf ?father.
    FILTER NOT EXISTS {
        ?son rdfs:subClassOf ?inter_type.
        ?inter_type rdfs:subClassOf ?father.
    }
    ?father skos:prefLabel ?fatherLabel.
    FILTER (langMatches( lang(?fatherLabel), "$lang" ) || langMatches( lang(?fatherLabel), "" )).
    ?son skos:prefLabel ?sonLabel.
    FILTER (langMatches( lang(?sonLabel), "$lang" ) || langMatches( lang(?sonLabel), "" )).
}
`;
