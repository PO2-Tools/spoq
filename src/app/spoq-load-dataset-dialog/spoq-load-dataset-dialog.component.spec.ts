import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqLoadDatasetDialogComponent } from './spoq-load-dataset-dialog.component';

describe('SpoqLoadDatasetDialogComponent', () => {
  let component: SpoqLoadDatasetDialogComponent;
  let fixture: ComponentFixture<SpoqLoadDatasetDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqLoadDatasetDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqLoadDatasetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
