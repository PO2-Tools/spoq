import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {SaveLoadService} from '../services/save-load.service';
import {SessionService} from '../services/session.service';
import {Dataverset} from '../model/Dataverset';

@Component({
  selector: 'app-spoq-load-dataset-dialog',
  templateUrl: './spoq-load-dataset-dialog.component.html',
  styleUrls: ['./spoq-load-dataset-dialog.component.css']
})
export class SpoqLoadDatasetDialogComponent implements OnInit {

  @Output() load = new EventEmitter<boolean>();

  save: string;
  type: string;
  saveDescription: string;
  loadError: boolean;
  saveSubject: string;
  saveAuthors: string;
  saveKeywords: string;
  saveCreation: string;
  saveModification: string;
  landing: string;
  url: string;
  title: string;
  datasetID: string;
  datasetDOI: string;
  allFolder: any;
  wholesave: string;

  constructor(private loadService: SaveLoadService,
              private sessionService: SessionService,
              private router: Router,
              public dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit() {
  }

  updateDescription(desc: any) {
    this.saveDescription = desc.description;
    this.saveSubject = desc.subjects;
    this.saveAuthors = desc.authors;
    this.saveKeywords = desc.keywords;
    this.saveCreation = desc.creation;
    this.saveModification = desc.modification;
    this.landing = desc.landing;
    this.url = desc.url;
    this.title = desc.title;
    this.datasetID = desc.datasetID;
    this.datasetDOI = desc.doi;
    this.allFolder = desc.folders;
    this.wholesave = null;
  }


  startLoading(): void {
    this.sessionService.datasetsLoading.push('dataverse');
    this.loadError = false;
    const temp = {save : this.wholesave};
    if ('none' !== this.wholesave) {
      this.loadService.loadWholeState(temp).subscribe(response => {
        this.load.emit(response);
        this.sessionService.datasetsLoading.splice(0, 1);
        if (response) {
          this.dialog.closeAll();
          // tslint:disable-next-line:max-line-length
          const dset = new Dataverset(this.title, this.saveDescription, this.saveAuthors, this.saveKeywords, this.saveSubject, this.landing, this.datasetID, this.datasetDOI, null, '');
          dset.allQuery = Object.keys(this.allFolder);
          this.sessionService.loadDataverset(dset);
        } else {
          this.dialog.closeAll();
          this.loadError = true;
        }
      });
    } else {
      // creating new query
      this.sessionService.datasetsLoading.splice(0, 1);
      // tslint:disable-next-line:max-line-length
      const dset = new Dataverset(this.title, this.saveDescription, this.saveAuthors, this.saveKeywords, this.saveSubject, this.landing, this.datasetID, this.datasetDOI, null, '');
      this.dialog.closeAll();
      this.sessionService.loadDataverset(dset);
    }
  }
}
