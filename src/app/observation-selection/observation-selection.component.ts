import {Component, Inject, OnInit} from '@angular/core';
import {Step} from '../model/Step';
import {FlatTreeControl} from '@angular/cdk/tree';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import {SessionService} from '../services/session.service';
import {Observation} from '../model/Observation';

interface FlatObservationNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-observation-selection',
  templateUrl: './observation-selection.component.html',
  styleUrls: ['./observation-selection.component.css']
})
export class ObservationSelectionComponent implements OnInit {

  observationSelected: Observation;
  treeControl = new FlatTreeControl<FlatObservationNode>(node => node.level, node => node.expandable);

  private transformer = (node: Observation, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level,
      step: node
    };
  }

  treeFlattener = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(public sessionsService: SessionService, @Inject(MAT_DIALOG_DATA) public data: any) { this.dataSource.data = data; }
  hasChild = (_: number, node: FlatObservationNode) => node.expandable;
  ngOnInit() {
  }

}
