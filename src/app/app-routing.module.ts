import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpoqFormComponent } from './spoq-form/spoq-form.component';
import { SpoqResultsPageComponent } from './spoq-results-page/spoq-results-page.component';
import { SpoqSparqlPageComponent } from './spoq-sparql-page/spoq-sparql-page.component';

const routes: Routes = [
  { path: '', redirectTo: '/form', pathMatch: 'full' },
  { path: 'form', component: SpoqFormComponent },
  { path: 'results', component: SpoqResultsPageComponent },
  { path: 'sparql', component: SpoqSparqlPageComponent },
  { path: '**', redirectTo: '/form' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
