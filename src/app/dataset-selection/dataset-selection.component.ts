import {Component, Inject, OnInit} from '@angular/core';
import {SessionService} from '../services/session.service';
import {Dataset} from '../model/Dataset';
import {NestedTreeControl} from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dataset-selection',
  templateUrl: './dataset-selection.component.html',
  styleUrls: ['./dataset-selection.component.css']
})
export class DatasetSelectionComponent implements OnInit {
  treeControl = new NestedTreeControl<Dataset>(node => Dataset[0]);
  dataSource = new MatTreeNestedDataSource<Dataset>();
  constructor(public sessionsService: SessionService, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.dataSource.data = data;
  }

  ngOnInit(): void {

  }
}
