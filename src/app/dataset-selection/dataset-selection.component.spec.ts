import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DatasetSelectionComponent } from './dataset-selection.component';

describe('DatasetSelectionComponent', () => {
  let component: DatasetSelectionComponent;
  let fixture: ComponentFixture<DatasetSelectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DatasetSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
