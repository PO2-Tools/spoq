import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StepSelectionComponent } from './step-selection.component';

describe('StepSelectionComponent', () => {
  let component: StepSelectionComponent;
  let fixture: ComponentFixture<StepSelectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StepSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
