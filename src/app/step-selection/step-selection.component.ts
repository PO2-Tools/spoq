import {Component, Inject, Input, OnInit} from '@angular/core';
import {SessionService} from '../services/session.service';
import {Process} from '../model/Process';
import {Step} from '../model/Step';
import {FlatTreeControl} from '@angular/cdk/tree';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';

interface FlatStepNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-step-selection',
  templateUrl: './step-selection.component.html',
  styleUrls: ['./step-selection.component.css']
})

export class StepSelectionComponent implements OnInit {

  stepSelected: Step;
  treeControl = new FlatTreeControl<FlatStepNode>(node => node.level, node => node.expandable);

  private transformer = (node: Step, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.type,
      level,
      step: node
    };
  }

  treeFlattener = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(public sessionsService: SessionService, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.dataSource.data = data;
  }

  hasChild = (_: number, node: FlatStepNode) => node.expandable;
  ngOnInit() {
  }

}
