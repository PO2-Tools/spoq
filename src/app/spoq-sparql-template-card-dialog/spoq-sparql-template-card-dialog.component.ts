import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-spoq-sparql-template-card-dialog',
  templateUrl: './spoq-sparql-template-card-dialog.component.html',
  styleUrls: ['./spoq-sparql-template-card-dialog.component.css']
})
export class SpoqSparqlTemplateCardDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }
}
