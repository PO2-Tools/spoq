import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqSparqlTemplateCardDialogComponent } from './spoq-sparql-template-card-dialog.component';

describe('SpoqSparqlTemplateCardDialogComponent', () => {
  let component: SpoqSparqlTemplateCardDialogComponent;
  let fixture: ComponentFixture<SpoqSparqlTemplateCardDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqSparqlTemplateCardDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqSparqlTemplateCardDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
