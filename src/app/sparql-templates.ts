export const PREFIX = `
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX PO2: <http://opendata.inrae.fr/PO2/core/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX time: <http://www.w3.org/2006/time#>
PREFIX sesame: <http://www.openrdf.org/schema/sesame#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX ssn: <http://www.w3.org/ns/ssn/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX core: <http://opendata.inrae.fr/PO2/core/>
PREFIX qudt: <http://qudt.org/schema/qudt/>
PREFIX schema: <http://schema.org/>
PREFIX dcat: <http://www.w3.org/ns/dcat#>


SELECT $selectList WHERE {
    [...]
}
`;

// SI process DO not filter $processVar = une variable sparql e.g ?processURI_1
// Sinon $processVar = URI du process selectionné

// SI step Do not filter ou type => $stepInd = une variable sparql e.g ?steoURI_1
// Sinon $stepInd = URI de l'étape selectionné

// SI step filtre sur type => $stepType = URI type de l'étape && $typeDirectType = rdf:type
// Sinon $stepType = une variable sparql && $typeDirectType = sesame:directType

// $stepIndLabel = variable sparql
// $processVarLabel = variable sparql
export const FILTER_STEP = `
$itineraryVar rdf:type $processVar.
$itineraryVar PO2:hasStep $stepInd.
$itineraryVar sesame:directType $directProcess.
optional {
  $itineraryVar skos:prefLabel $itineraryVarLabel.
  $directProcess skos:prefLabel $processVarLabel.
  FILTER (langMatches( lang($processVarLabel), "$lang" ) || langMatches( lang($processVarLabel), "" )).
}

optional {
   $directProcess ^dcat:dataset/skos:prefLabel $projectVarName .
}
optional {
  $stepInd skos:prefLabel $stepIndLabel.
  FILTER (langMatches( lang($stepIndLabel), "$lang" ) || langMatches( lang($stepIndLabel), "" )).
 }
$stepInd $typeDirectType $stepType.
`;

// même $stepInd que pour l'étape
// $samplerInd = variable sparql
// $samplerType = URI du materiel
export const FILTER_STEP_MAT = `
$stepInd sosa:madeByActuator $actuatorInd.
$actuatorInd ^ssn:hasSubSystem $materialInd.
$materialInd rdf:type $materialType.
optional {
  $materialInd skos:prefLabel $materialIndLabel.
  FILTER (langMatches( lang($materialIndLabel), "$lang" ) || langMatches( lang($materialIndLabel), "" )).
}
`;

// même $samplerInd que pour l'étape_materiel
// $propInd = variable sparql
// $propType = URI de la propriété
export const FILTER_STEP_MAT_PARAM = `
$actuatorInd ssn:hasProperty $result.
$result sesame:directType $propType.
optional {
  $propType skos:prefLabel $propIndLabel.
  FILTER (langMatches( lang($propIndLabel), "$lang" ) || langMatches( lang($propIndLabel), "" )).
}
`;


// même $stepInd que pour l'étape
// $property -> si input alors PO2:hasInput
//           -> si output alors PO2:hasOutput
// $inputOutput = variable sparql
// $component = URI du component
export const FILTER_STEP_INPUT_OUTPUT = `
$stepInd $property $inputOutput.
$inputOutput rdf:type $component.
optional {
  $inputOutput skos:prefLabel $inputOutputLabel.
  FILTER (langMatches( lang($inputOutputLabel), "$lang" ) || langMatches( lang($inputOutputLabel), "" )).
}
`;

// même $inputOutput que pour FILTER_STEP_INPUT_OUTPUT
// $ingredient = variable sparql
// $ingredientType = URI de l'ingredient selectionné
// $result = variable sparql
export const FILTER_STEP_INPUT_OUTPUT_INGR = `
$inputOutput PO2:isComposedOf $ingredient.
$ingredient rdf:type $ingredientType.
$ingredient ssn:hasProperty $result.
optional {
  $ingredient skos:prefLabel $ingredientLabel.
  FILTER (langMatches( lang($ingredientLabel), "$lang" ) || langMatches( lang($ingredientLabel), "" )).
}
`;

// même result que FILTER_STEP_INPUT_OUTPUT_INGR || FILTER_STEP_PROP || FILTER_STEP_MAT_PARAM
// $valueMin = variable sparql
// $valueMax = variable sparql
// export const FILTER_QUANTITY = `
// $result core:hasMaxSupport $valueMax.
// $result core:hasMinSupport $valueMin.
// $result qudt:unit $unit.
// $unit skos:prefLabel $unitLabel.
// `;

export const FILTER_QUANTITY = `
$result schema:maxValue $valueMax.
$result schema:minValue $valueMin.
$result schema:unitCode $unitLabel.
$result schema:value $valueOrigin.
$result schema:unitText $unitOrigin.
`;

// même result que FILTER_STEP_INPUT_OUTPUT_INGR || FILTER_STEP_PROP || FILTER_STEP_MAT_PARAM
// $value = variable sparql
export const FILTER_QUALITY = `
$result schema:value $value.
`;

// même $result que pour FILTER_STEP_INPUT_OUTPUT_INGR
// $valueMax & $valueMin = variable sparql
// $val valeur saisie
export const FILTER_QUANTITY_EXACT = `
filter($unitLabel="$unitValue").
filter($valueMax >= "$val"^^xsd:double && $valueMin <= "$val"^^xsd:double).
`;

// même $result que pour FILTER_STEP_INPUT_OUTPUT_INGR
// $valueMax & $valueMin = variable sparql
// $val valeur saisie
export const FILTER_QUANTITY_MIN = `
filter($unitLabel="$unitValue").
filter($valueMax >= "$val"^^xsd:double).
`;

// même $result que pour FILTER_STEP_INPUT_OUTPUT_INGR
// $valueMax & $valueMin = variable sparql
// $val valeur saisie
export const FILTER_QUANTITY_MAX = `
filter($unitLabel="$unitValue").
filter($valueMin <= "$val"^^xsd:double).
`;

// PERTINENCE ????
// même $result que pour FILTER_STEP_INPUT_OUTPUT_INGR
// $value = variable sparql
// $val valeur saisie
export const FILTER_QUALITY_FILTER = `
filter regex($value , "$val" ,"i").
`;

// même $obsInd que pour FILTER_STEP_OBS
// $result = variable sparql
// $propInd = variable sparql
// $propType = URI de la propriété
// $propLabel = variable sparql
// can't use sosa:hasMember+ -> graphdb does'nt reply
export const FILTER_STEP_PROP = `
$obsInd PO2:hasObservationCollection $collectionTop .
$collectionTop (sosa:hasMember|(sosa:hasMember/sosa:hasMember))/sosa:hasResult $result .
$collectionInd sosa:hasMember/sosa:hasResult $result.
$result ^sosa:hasResult/sosa:observedProperty/rdf:type $propType .
optional {
  $result skos:prefLabel $propLabel.
  FILTER (langMatches( lang($propLabel), "$lang" ) || langMatches( lang($propLabel), "" )).
}
`;

export const FILTER_OBS_PROP_OBJECT = `
  $result ^sosa:hasResult/sosa:observedProperty/PO2:hasObjectOfInterest $objectType.
  optional {
    $objectType skos:prefLabel $objectLabel.
    FILTER (langMatches( lang($objectLabel), "$lang" ) || langMatches( lang($objectLabel), "" )).
  }
`;

// ajoute les objets dans les résultats.
// $result = variable sparql
// $object = variable sparql
export const ADD_OBJ_OBS_PROP = `
optional {
  $result ^sosa:hasResult/sosa:observedProperty/PO2:hasObjectOfInterest $objectInd.
  optional {
    $objectInd skos:prefLabel $objectLabel.
    FILTER (langMatches( lang($objectLabel), "$lang" ) || langMatches( lang($objectLabel), "" )).
  }
}
`;

// même stepInd que pour FILTER_STEP
// $obsInd = variable sparql
// $obsLabel = variable sparql
// $obsType = type de l'observation
export const FILTER_STEP_OBS = `
$stepInd sosa:phenomenonTime / time:intervalContains /  ^sosa:phenomenonTime $obsInd.
optional {
  $obsInd skos:prefLabel $obsLabel.
  FILTER (langMatches( lang($obsLabel), "$lang" ) || langMatches( lang($obsLabel), "" )).
}
`;

// même $obsInd que pour FILTER_STEP_PROP
// $sensorInd = variable sparql
// $sensorType = URI du materiel
// $sensorIndLabel = variable sparql
export const FILTER_STEP_PROP_MAT = `
$obsInd PO2:hasObservationCollection/sosa:madeBySensor/^ssn:hasSubSystem $sensorInd.
$sensorInd rdf:type $sensorType.
optional {
  $sensorInd skos:prefLabel $sensorIndLabel.
  FILTER (langMatches( lang($sensorIndLabel), "$lang" ) || langMatches( lang($sensorIndLabel), "" )).
}
`;

//////////////////////////////// ARBRE DES POSSIBILITEES////////////////////////
// FILTER_STEP
//         |_  FILTER_STEP_MAT
//                    |_ FILTER_STEP_MAT_PARAM + (FILTER_QUALITY               ||          FILTER_QUANTITY )
//                                                        |_ FILTER_QUALITY_FILTER                   |_ FILTER_QUANTITY_EXACT || FILTER_QUANTITY_MIN || FILTER_QUANTITY_MAX
//         |_ FILTER_STEP_INPUT_OUTPUT
//                    |_ FILTER_STEP_INPUT_OUTPUT_INGR + (FILTER_QUALITY               ||          FILTER_QUANTITY )
//                                                        |_ FILTER_QUALITY_FILTER                   |_ FILTER_QUANTITY_EXACT || FILTER_QUANTITY_MIN || FILTER_QUANTITY_MAX
//         |_ FILTER_STEP_PROP + (FILTER_QUALITY               ||          FILTER_QUANTITY )
//                                  |_ FILTER_QUALITY_FILTER                   |_ FILTER_QUANTITY_EXACT || FILTER_QUANTITY_MIN || FILTER_QUANTITY_MAX
//                    |_ FILTER_STEP_PROP_MAT
///////////////////////////////////////////////////////////////////////////////////
