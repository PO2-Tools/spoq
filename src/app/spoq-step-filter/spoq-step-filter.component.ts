import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Filter } from '../model/Filter';
import { InitDataService } from '../services/init-data.service';
import { Measurement, IN_COMP, OUT_COMP, STEP_MAT, OBS_MAT } from '../model/Measurement';
import {SessionService} from '../services/session.service';
import {Step} from '../model/Step';
import {Process} from '../model/Process';

@Component({
  selector: 'app-spoq-step-filter',
  templateUrl: './spoq-step-filter.component.html',
  styleUrls: ['./spoq-step-filter.component.css']
})
export class SpoqStepComponentComponent implements OnInit {
  @Input() title: string;
  @Input() selectedProcess: Process;
  @Input() selectedStep: Step;
  @Input() availableComponent: Filter[];
  @Input() componentType: string;
  selectedComponent: Filter;
  measurementName: string;

  doNotFilterFilter: Filter;
  doNotFilterMeasurement: Measurement;

  constructor(public initDataService: InitDataService) { }

  test() {
    console.log(this.selectedComponent);
  }

  ngOnInit() {
    this.doNotFilterFilter = new Filter('doNotFilter', '');
    this.doNotFilterMeasurement = new Measurement('doNotFilter', '', null, [], '');

    if (this.title === IN_COMP || this.title === OUT_COMP) {
      this.measurementName = 'Ingredient';
    } else if (this.title === STEP_MAT || this.title === OBS_MAT) {
      this.measurementName = 'Control parameter';
    }
    this.selectedComponent = this.selectedStep.filter[this.componentType];
  }

  compareObjects(o1: any, o2: any): boolean {
    if (o1 == null || o2 == null) {
      return false;
    }
    return o1.name === o2.name;
  }

  componentSelected(component: Filter): void {
    if (component) { // only if not "unselect"
      this.selectedComponent =  {...component} as Filter;
      this.getFormData(this.selectedComponent);
    } else {
      this.selectedComponent = null;
    }
    this.selectedStep.filter[this.componentType] = this.selectedComponent;
    console.log(this.selectedComponent);
  }

  getFormData(component: Filter): void {
    if (this.title === IN_COMP || this.title === OUT_COMP) {
      this.initDataService.getComponentIngredients(this.selectedProcess, this.selectedStep, component, this.title === IN_COMP)
        .subscribe(ingredients => component.measurementsAvailable = ingredients);
    } else if (this.title === STEP_MAT || this.title === OBS_MAT) {
      this.initDataService.getStepMaterialParams(this.selectedProcess, this.selectedStep, component)
        .subscribe(params => component.measurementsAvailable = params);
    }
  }

  valueTyped(): void {
    if (!this.selectedComponent.measurement.boundary) {
      this.selectedComponent.measurement.boundary = 'exact';
    }
  }
}
