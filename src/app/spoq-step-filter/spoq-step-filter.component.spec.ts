import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqStepComponentComponent } from './spoq-step-filter.component';

describe('SpoqStepComponentComponent', () => {
  let component: SpoqStepComponentComponent;
  let fixture: ComponentFixture<SpoqStepComponentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SpoqStepComponentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqStepComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
