import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqFormatOptionsComponent } from './spoq-format-options.component';

describe('SpoqFormatOptionsComponent', () => {
  let component: SpoqFormatOptionsComponent;
  let fixture: ComponentFixture<SpoqFormatOptionsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqFormatOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqFormatOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
