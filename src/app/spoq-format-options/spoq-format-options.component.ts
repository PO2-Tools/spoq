import { Component, OnInit, Inject } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { ResultsFormatConverterService } from '../services/results-format-converter.service';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-spoq-format-options',
  templateUrl: './spoq-format-options.component.html',
  styleUrls: ['./spoq-format-options.component.css']
})
export class SpoqFormatOptionsComponent implements OnInit {

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any[],
    private bottomSheetRef: MatBottomSheetRef<SpoqFormatOptionsComponent>,
    public resultFormatConverter: ResultsFormatConverterService,
  ) { }

  ngOnInit() {
  }

  chooseFormat(format: string): void {
    console.log(format);
    let stringData: string;

    if (format === 'JSON') {
      stringData = JSON.stringify(this.data);
    } else if (format === 'TSV') {
      stringData = this.resultFormatConverter.convertToTSV(this.data);
    }

    this.downloadResults(stringData, format);
    this.bottomSheetRef.dismiss();
  }

  downloadResults(res: string, format: string): void {
    const element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(res));
    element.setAttribute('download', 'results.' + format.toLocaleLowerCase());

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
}
