import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqSaveDialogComponent } from './spoq-save-dialog.component';

describe('SpoqSaveDialogComponent', () => {
  let component: SpoqSaveDialogComponent;
  let fixture: ComponentFixture<SpoqSaveDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqSaveDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqSaveDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
