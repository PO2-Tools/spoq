import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {SaveLoadService} from '../services/save-load.service';
import {SessionService} from '../services/session.service';

@Component({
  selector: 'app-spoq-save-dialog',
  templateUrl: './spoq-save-dialog.component.html',
  styleUrls: ['./spoq-save-dialog.component.css']
})
export class SpoqSaveDialogComponent implements OnInit {
  type: string;
  saveName: string;
  saveDesc: string;
  errorSave: boolean;

  constructor(private saveService: SaveLoadService,
              public dialog: MatDialog,
              private sessionsService: SessionService,
              @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.type = 'form';
    this.errorSave = false;
  }

  startSave(result: any) {
    const session = this.sessionsService;
    if (!result.description) {
      result.description = '';
    }
    if (result.type === 'form') {
      this.saveService.saveFormState(session.loadCachedFormSelection(), result.name, result.description).subscribe(res => {
        if (res) {
          this.errorSave = false;
          this.dialog.closeAll();
        } else {
          this.errorSave = true;
        }
      });  // return boolean, if false -> name is already taken
    } else if (result.type === 'sparql') {
      this.saveService.saveEditorState(session.editorContent, session.selectedEditorDatasets, result.name, result.description)
        .subscribe(res => {
          if (res) {
            this.errorSave = false;
            this.dialog.closeAll();
          } else {
            this.errorSave = true;
          }
        });  // false -> this.openSaveDialog(false, true);
    } else if (result.type === 'both') {
      this.saveService.saveBoth(session.editorContent, session.selectedParameters, result.name, result.description, session.selectedEditorDatasets)
        .subscribe(res => {
          if (res) {
            this.errorSave = false;
            this.dialog.closeAll();
          } else {
            this.errorSave = true;
          }
        });
    }
  }

}
