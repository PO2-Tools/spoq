import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqWarningDialogComponent } from './spoq-warning-dialog.component';

describe('SpoqWarningDialogComponent', () => {
  let component: SpoqWarningDialogComponent;
  let fixture: ComponentFixture<SpoqWarningDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqWarningDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqWarningDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
