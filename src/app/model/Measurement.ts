export class Measurement {
  name: string;
  attribut: string;
  unit: string;
  availableUnits: string[];
  value: string;
  boundary: string;
  uri: string;

  constructor(n: string, att: string, un: string, aun: string[],  uri: string) {
    this.name = n;
    this.attribut = att;
    this.unit = un;
    this.availableUnits = aun;
    this.uri = uri;
  }
}

export const IN_COMP = 'Input component';
export const OUT_COMP = 'Output component';
export const STEP_MAT = 'Step Material';
export const OBS_MAT = 'Observation Material';
