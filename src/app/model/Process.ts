import { Step } from './Step';

export class Process {
  name: string;
  uri: string;
  type: string;
  typeName: string;
  steps: Step[];
  stepsTree: Step[];
  children: Process[]; // liste des types process enfant

  constructor(n: string, u: string, tn: string, t: string) {
    this.name = n;
    this.uri = u;
    this.typeName = tn;
    this.type = t;
    this.steps = [];
    this.stepsTree = [];
    this.children = [];
  }

  equals(p: Process): boolean {
    return this.name === p.name && this.uri === p.uri && this.type === p.type && this.typeName === p.typeName;
  }

  flatten(): Process[] {
    const retour: Process[] = [];
    retour.push(this);
    this.children.forEach(proc => {
      retour.push(...proc.flatten());
    });
    return retour;
  }
}
