import {Property} from './Property';
import {ObservationMaterial} from './ObservationMaterial';

export class Observation {
  name: string;
  uri: string;
  children: Observation[];
  properties: Property[];
  availableProperties: Property[];
  availableMaterial: ObservationMaterial[];
  includeMaterial: ObservationMaterial[];


  constructor(n: string, u: string) {
    this.name = n;
    this.uri = u;
    this.children = [];
    this.properties = [];
    this.availableProperties = [];
    this.availableMaterial = [];
    this.includeMaterial = [];
  }
}
