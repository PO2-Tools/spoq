import { Process } from './Process';

export class Dataset {
  name: string;
  processes: Process[];
  processTree: Process[];
  group: boolean;
  location: string;

  constructor(n: string, g: boolean, l: string) {
    this.name = n;
    this.processes = [];
    this.group = g;
    this.location = l;
    this.processTree = [];
  }
}
