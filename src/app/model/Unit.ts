export class Unit {
  code: string;
  pretty: string;
  group: string;

  constructor(c: string, p: string) {
    this.code = c;
    this.pretty = p;
    this.group = c + ' ' + p;
  }
}
