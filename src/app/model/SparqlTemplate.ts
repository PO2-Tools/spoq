export class SparqlTemplate {
    title: string;
    sparql: string;
    subTemplates: SparqlTemplate[];

    constructor(title: string, sparql: string) {
        this.title = title;
        this.sparql = sparql;
        this.subTemplates = [];
    }
}