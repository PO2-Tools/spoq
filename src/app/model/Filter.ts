import { Measurement } from './Measurement';

export class Filter {
  public static readonly MATERIAL = 'MATERIAL';
  public static readonly INPUTCOMPONENT = 'INPUT-COMPONENT';
  public static readonly OUTPUTCOMPONENT = 'OUTPUT-COMPONENT';
  name: string;
  uri: string;
  measurementsAvailable: Measurement[];
  measurement: Measurement;

  constructor(n: string, u: string) {
    this.name = n;
    this.uri = u;
    this.measurementsAvailable = [];
    this.measurement = null;
  }
}
