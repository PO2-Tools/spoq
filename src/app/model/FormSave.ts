import { FormParameters } from './FormParameters';

export class FormSave {
    name: string;
    save: FormParameters;
}