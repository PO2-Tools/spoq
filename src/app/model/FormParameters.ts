import { Dataset } from './Dataset';
import { Process } from './Process';
import { Step } from './Step';

export class FormParameters {
    /* all dataset that is selected in form*/
    selectedDatasets: Dataset[];
    /*the dataset to query. If more than one dataset in the form, a federated dataset is created */
    selectedDataset: Dataset;
    selectedProcess: Process;
    selectedSteps: Step[];

    constructor() {
        this.selectedDataset = null;
        this.selectedDatasets = [];
        this.selectedSteps = [];
    }
}
