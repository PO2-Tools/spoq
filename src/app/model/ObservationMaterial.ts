import { ObsControlParam } from './ObsControlParam';

export class ObservationMaterial {
    name: string;
    uri: string;
    obsContParam: ObsControlParam[];

    constructor(n: string, u: string) {
        this.name = n;
        this.uri = u;
        this.obsContParam = [];
    }
}