import { ObservationMaterial } from './ObservationMaterial';
import {ObjectOI} from './ObjectOI';

export class Property {
  name: string;
  uri: string;
  units: string;
  availableUnits: string[];
  numValue: string;
  boundary: string;
  showObject: boolean;
  // observationMaterials: ObservationMaterial[];
  availableObjects: ObjectOI[];
  includeObjects: ObjectOI[];
  excludeObjects: ObjectOI[];

  constructor(n: string, u: string) {
    this.name = n;
    this.uri = u;
    this.units = null;
    this.availableUnits = [];
    // this.observationMaterials = [];
    this.showObject = true;
    this.availableObjects = [];
    this.includeObjects = [];
    this.excludeObjects = [];
  }
}
