// this is a dataset for the INRAE dataverse
import {WholeSave} from './WholeSave';

export class Dataverset {
  title: string;
  description: string;
  authors: string;
  keywords: string;
  subjects: string;
  landingpage: string;
  files: File[];
  save: WholeSave;
  doi: string;
  datasetID: string;
  filePrefix: string;
  allQuery: string[];

  constructor(t: string, d: string, a: string, k: string, s: string, l: string, di: string, ddoi: string, save: WholeSave, filePrefix: string) {
    this.title = t;
    this.description = d;
    this.authors = a;
    this.keywords = k;
    this.subjects = s;
    this.landingpage = l;
    this.files = [];
    this.save = save;
    this.doi = ddoi;
    this.datasetID = di;
    this.filePrefix = filePrefix;
  }

  addFile(f: File) {
    this.files.push(f);
  }
}
