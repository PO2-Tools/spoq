import { Dataset } from './Dataset';

export class EditorSave {
    name: string;
    selectedDatasetsForEditor: Dataset[];
    editorSave: string;
}