import {EditorSave} from './EditorSave';
import {FormSave} from './FormSave';

export class WholeSave {
    name: string;
    formSave: FormSave;
    editorSave: EditorSave;
}
