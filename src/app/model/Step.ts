import { Filter } from './Filter';
import { Property } from './Property';
import {Observation} from './Observation';
import {Observable} from 'rxjs';

export class Step {
  uri: string;
  name: string;
  typeUri: string;
  type: string;
  doNotFilter: boolean;
  route: string;
  filterAvailable: Map<string, Filter[]>;
  filter: Map<string, Filter>;
  properties: Property[];
  observations: Observation[];
  observationsAvailable: Observation[];
  children: Step[];

  showSubResults: boolean;

  constructor(u: string, n: string, tu: string, t: string) {
    this.uri = u;
    this.name = n;
    this.typeUri = tu;
    this.type = t;
    this.filterAvailable = new Map();
    this.filterAvailable[Filter.MATERIAL] = [];
    this.filterAvailable[Filter.INPUTCOMPONENT] = [];
    this.filterAvailable[Filter.OUTPUTCOMPONENT] = [];
    this.filter = new Map();
    this.filter[Filter.MATERIAL] = null;
    this.filter[Filter.INPUTCOMPONENT] = null;
    this.filter[Filter.OUTPUTCOMPONENT] = null;
    // this.filter.set(Filter.MATERIAL, null);
    // this.filter.set(Filter.INPUTCOMPONENT, null);
    // this.filter.set(Filter.OUTPUTCOMPONENT, null);
    this.properties = [];
    this.observations = [];
    this.observationsAvailable = [];
    this.children = [];
    this.showSubResults = false;
  }

    copyFrom(step: Step) {
      this.uri = step.uri;
      this.name = step.name;
      this.typeUri = step.typeUri;
      this.type = step.type;
      this.filter[Filter.MATERIAL] = step.filter[Filter.MATERIAL];
      this.filter[Filter.INPUTCOMPONENT] = step.filter[Filter.INPUTCOMPONENT];
      this.filter[Filter.OUTPUTCOMPONENT] = step.filter[Filter.OUTPUTCOMPONENT];
      this.properties = [];
      this.observations = [];
      this.children = step.children;
      this.showSubResults = step.showSubResults;
    }
}
