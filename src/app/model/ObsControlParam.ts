export class ObsControlParam {
    name: string;
    uri: string;

    constructor(n: string, u: string) {
        this.name = n;
        this.uri = u;
    }
}