import { Component, OnInit } from '@angular/core';
import { SessionService } from '../services/session.service';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import {InitDataService} from '../services/init-data.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-spoq-login-page',
  templateUrl: './spoq-login-page.component.html',
  styleUrls: ['./spoq-login-page.component.css']
})
export class SpoqLoginPageComponent implements OnInit {
  usrName: string;
  password: string;
  wrongCredentials: boolean;
  goodCredentials: boolean;
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private sessionService: SessionService,
    private router: Router,
    private dialogRef: MatDialogRef<SpoqLoginPageComponent>,
    private initDataService: InitDataService
  ) { }

  ngOnInit() {
    this.wrongCredentials = false;
    this.goodCredentials = false;
    this.form = this.fb.group({
      login: '',
      password: ''
    });
  }

  login(): void {
    this.sessionService.login(this.usrName, this.password).subscribe(loggedIn => {
      this.wrongCredentials = !loggedIn;
      this.goodCredentials = loggedIn;
      if (loggedIn) {
        this.dialogRef.close();
        this.sessionService.loggedIn = loggedIn;
        this.initDataService.getInitData().subscribe(data => {
              this.sessionService.reset();
              this.sessionService.availableDatasets = data;
        });
      }
    });
  }
}
