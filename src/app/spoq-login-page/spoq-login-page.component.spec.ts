import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SpoqLoginPageComponent } from './spoq-login-page.component';

describe('SpoqLoginPageComponent', () => {
  let component: SpoqLoginPageComponent;
  let fixture: ComponentFixture<SpoqLoginPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SpoqLoginPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpoqLoginPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
